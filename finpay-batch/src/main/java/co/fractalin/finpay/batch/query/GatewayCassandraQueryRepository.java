package co.fractalin.finpay.batch.query;

import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.convert.CassandraConverter;
import org.springframework.data.cassandra.core.mapping.CassandraPersistentEntity;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;
import org.springframework.data.repository.Repository;

/**
 * Repository générique servant de base pour les repositories
 * assurant la lecture des entités du pcnv2-gateway
 *
 * @param <T> entité
 * @param <ID> classe d'identifiant de l'entité
 */
public abstract class GatewayCassandraQueryRepository<T, ID> implements Repository<T, ID> {

  private final CassandraOperations cassandraTemplate;
  private final CassandraEntityInformation<T, ID> entityInformation;

  public GatewayCassandraQueryRepository(
          final CassandraOperations cassandraTemplate,
          final CassandraEntityInformation<T, ID> entityInformation) {
    this.cassandraTemplate = cassandraTemplate;
    this.entityInformation = entityInformation;
  }

  protected CassandraPersistentEntity<?> getPersistentEntity() {
    return getConverter()
            .getMappingContext()
            .getRequiredPersistentEntity(entityInformation.getJavaType());
  }

  protected CassandraConverter getConverter() {
    return cassandraTemplate.getConverter();
  }
}
