package co.fractalin.finpay.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class FinpayBatchApplication {
  public static void main(String[] args) {
    SpringApplication.run(FinpayBatchApplication.class, args);
  }
}
