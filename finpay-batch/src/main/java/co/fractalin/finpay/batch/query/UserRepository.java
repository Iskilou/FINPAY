package co.fractalin.finpay.batch.query;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select.Where;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import co.fractalin.finpay.commons.exception.NotFoundException;
import co.fractalin.finpay.model.security.User;
import java.util.List;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.cql.CqlOperations;
import org.springframework.data.cassandra.core.mapping.CassandraPersistentEntity;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Repository des utilisateurs
 */
public class UserRepository extends GatewayCassandraQueryRepository<User, String> {

  private final CqlOperations template;
  private final CassandraEntityInformation<User, String> entityInformation;
  private final String keyspace;
  private final ObjectMapper mapper;

  public UserRepository(CassandraOperations cassandraTemplate,
          CqlOperations template,
          CassandraEntityInformation<User, String> entityInformation,
          String keyspace,
          ObjectMapper mapper) {
    super(cassandraTemplate, entityInformation);
    this.template = template;
    this.entityInformation = entityInformation;
    this.keyspace = keyspace;
    this.mapper = mapper;
  }

  public User findById(String id) {
    assert id != null;
    final CassandraPersistentEntity<?> persistentEntity = getPersistentEntity();
    final Where whereClause
            = QueryBuilder.select()
                    .from(keyspace, persistentEntity.getTableName().toCql())
                    .allowFiltering()
                    .where(QueryBuilder.eq("id", id));

    Map<String, Object> map = template.queryForMap(whereClause);
    return Optional.ofNullable(mapper.convertValue(map, entityInformation.getJavaType()))
            .orElseThrow(() -> new NotFoundException(User.class, id));
  }

  public User findByUserId(String id) {
    assert id != null;
    final CassandraPersistentEntity<?> persistentEntity = getPersistentEntity();
    final Where whereClause
            = QueryBuilder.select()
                    .from(keyspace, persistentEntity.getTableName().toCql())
                    .allowFiltering()
                    .where(QueryBuilder.eq("id", id));

    Map<String, Object> map = template.queryForMap(whereClause);
    return Optional.ofNullable(mapper.convertValue(map, entityInformation.getJavaType()))
            .orElseThrow(() -> new NotFoundException(User.class, id));
  }

  public List<User> findByPcnOrganizationIdAndRole(String pcnOrganizationId, String role) {
    final CassandraPersistentEntity<?> persistentEntity = getPersistentEntity();
    Where where = QueryBuilder
            .select()
            .from(keyspace, persistentEntity.getTableName().toCql())
            .allowFiltering()
            .where();

    if (!Strings.isNullOrEmpty(pcnOrganizationId)) {
      where.and(QueryBuilder.eq("pcn_organization_id", pcnOrganizationId));
    }

    if (!Strings.isNullOrEmpty(role)) {
      where.and(QueryBuilder.eq("role", role));
    }

    return template.queryForList(where)
            .stream()
            .map(map -> mapper.convertValue(map, entityInformation.getJavaType()))
            .collect(Collectors.toList());
  }
}
