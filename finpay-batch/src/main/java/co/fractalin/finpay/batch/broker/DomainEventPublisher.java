package co.fractalin.finpay.batch.broker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import co.fractalin.finpay.commons.dto.event.SendingEmailEvent;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.StreamSupport.stream;

@Service
public class DomainEventPublisher {
  private final String topic;
  private final KafkaProducer<String, String> persistentEventProducer;
  private final ObjectMapper objectMapper;

  public DomainEventPublisher(@Value("${application.kafka.notification.topic}") String topic,
          KafkaProducer<String, String> persistentEventProducer, ObjectMapper objectMapper) {
    this.topic = topic;
    this.persistentEventProducer = persistentEventProducer;
    this.objectMapper = objectMapper;
  }

  @LogArgumentsAndResult
  public void createEmailSendingEvent(SendingEmailEvent emailEvent) {
    Headers headers = new RecordHeaders();
    headers.add("id", UUID.randomUUID().toString().getBytes());
    headers.add("type", emailEvent.getType().getBytes());
    headers.add("timestamp", Timestamp.valueOf(LocalDateTime.now()).toString().getBytes());

    Iterable<Header> iterable = stream(headers.spliterator(), false)
            .collect(Collectors.toList());

    try {
      this.persistentEventProducer.send(new ProducerRecord<>(
              this.topic, null, null, emailEvent.getType(), objectMapper.writeValueAsString(emailEvent), iterable));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
