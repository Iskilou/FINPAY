package co.fractalin.finpay.web.commons.swagger;

import com.google.common.base.Optional;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;

import java.lang.annotation.Annotation;
import java.util.stream.Stream;

/**
 * Définit un paramètre comme obligatoire
 * s'il porte l'annotation {@link PathVariable}, {@link RequestBody} ou {@link RequestParam#required()}
 */
@Order
@Component
public class RequiredParameterBuilder implements ParameterBuilderPlugin {
  @Override
  public void apply(ParameterContext parameterContext) {
    parameterContext.parameterBuilder().required(RequiredParameterBuilder.isRequired(parameterContext.resolvedMethodParameter()));
  }

  private static boolean isRequired(ResolvedMethodParameter resolvedMethodParameter) {
    return hasAnnotation(resolvedMethodParameter, PathVariable.class, RequestBody.class) || resolvedMethodParameter.findAnnotation(RequestParam.class)
        .transform(RequestParam::required)
        .or(false);
  }

  @SafeVarargs
  private static boolean hasAnnotation(ResolvedMethodParameter resolvedMethodParameter, Class<? extends Annotation>... annotations) {
    return Stream.of(annotations)
        .map(resolvedMethodParameter::findAnnotation)
        .anyMatch(Optional::isPresent);
  }

  @Override
  public boolean supports(DocumentationType documentationType) {
    return true;
  }
}
