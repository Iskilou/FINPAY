package co.fractalin.finpay.web.commons.security.model;

import co.fractalin.finpay.model.enumeration.Role;
import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.web.commons.security.ParsedToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.core.convert.ConversionService;

import static io.jsonwebtoken.Jwts.claims;

/**
 * Objet qui représente un token parsé de l'API PCN
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FinpayParsedToken extends ParsedToken {

  /* nom des claims spécifiques utilisées dans le token */
  public static final String USERNAME = "username";
  public static final String FIRSTNAME = "firstname";
  public static final String LASTNAME = "lastname";
  public static final String ORGANIZATION = "Organization";
  public static final String ROLE = "role";
  public static final String ORGANIZATION_NAME = "organizationName";

  private final String username;
  private final String firstname;
  private final String lastname;
  private final String organization;
  private final Role role;
  private final String organizationName;


  public FinpayParsedToken(String token, String secret, ConversionService conversionService) {
    super(token, secret, conversionService);

    username = get(USERNAME, String.class);
    firstname = get(FIRSTNAME, String.class);
    lastname = get(LASTNAME, String.class);
    organization = get(ORGANIZATION, String.class);
    role = get(ROLE, Role.class);
    organizationName = get(ORGANIZATION_NAME, String.class);
  }

  public FinpayParsedToken(ConversionService conversionService) {
    super(claims(), conversionService);

    username = null;
    firstname = null;
    lastname = null;
    organization = null;
    role = null;
    organizationName = null;
  }
}
