package co.fractalin.finpay.web.commons.utils;

import io.jsonwebtoken.Jwts;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static lombok.AccessLevel.PRIVATE;

/**
 * Regroupe les méthodes utilitaires pour JWT
 */
@NoArgsConstructor(access = PRIVATE)
public class JwtUtils {

  public static String createJwtToken(String subject, Date expirationDate, Map<String, Object> claims, String secret) {
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(subject)
        .setExpiration(expirationDate)
        .signWith(HS256, secret)
        .compact();
  }

  public static Date getExpirationDate(ZonedDateTime date) {
    return Date.from(Instant.from(date));
  }

}
