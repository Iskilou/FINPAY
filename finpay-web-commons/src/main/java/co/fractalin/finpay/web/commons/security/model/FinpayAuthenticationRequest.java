package co.fractalin.finpay.web.commons.security.model;

import co.fractalin.finpay.commons.dto.security.AuthenticationRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.*;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.USER_ID;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.ORGANIZATION_ID;


/**
 * DTO utilisé par la requête d'authentification
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class FinpayAuthenticationRequest extends AuthenticationRequest {



  @ApiModelProperty(USER_ID)
  private String userId;

  @ApiModelProperty(ORGANIZATION_ID)
  private String organizationId;

  @Builder
  public FinpayAuthenticationRequest(String username, String password,  String userId, String organizationId) {
    super(username, password);
    this.userId = userId;
    this.organizationId = organizationId;
  }
}
