package co.fractalin.finpay.web.commons.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.Getter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.time.ZonedDateTime;
import java.util.Set;

import static co.fractalin.finpay.commons.utils.DateUtils.toZonedDateTime;
import static java.time.Instant.now;

/**
 * Classe réprésentant un token parsé
 */
public abstract class ParsedToken {
  @Getter
  private Claims claims;
  private ConversionService conversionService;

  public ParsedToken(Claims claims, ConversionService conversionService) {
    this.claims = claims;
    this.conversionService = conversionService;
  }

  protected ParsedToken(String token, String secret, ConversionService conversionService) {
    this(Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody(), conversionService);
  }

  public String getUsername() {
    return claims.getSubject();
  }

  public ZonedDateTime getExpirationDate() {
    return toZonedDateTime(claims.getExpiration());
  }

  public boolean hasExpired() {
    return claims.getExpiration().toInstant().isBefore(now());
  }

  protected final <T> T get(String claim, Class<T> targetClass) {
    return conversionService.convert(claims.get(claim), targetClass);
  }

  protected final <T> Set<T> getSet(String claim, Class<T> targetElementClass) {
    Object source = claims.get(claim);
    return (Set<T>) conversionService
        .convert(source,
            TypeDescriptor.forObject(source),
            TypeDescriptor.collection(Set.class,
                TypeDescriptor.valueOf(targetElementClass)));
  }
}
