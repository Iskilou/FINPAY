package co.fractalin.finpay.web.commons.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Filtrage : uniquement les appels avec un jeton valide sont acceptés
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthenticationTokenFilter<T extends ParsedToken> implements Filter {

  private final PublicRoutesMatcher publicRoutesMatcher;
  private final WebTokenService<T, ?> tokenService;

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    String authToken = tokenService.getToken(request);

    if (isNotEmpty(authToken)) {
      try {
        SecurityContextHolder.getContext().setAuthentication(tokenService.getAuthentication(tokenService.parse(authToken)));
      }catch (ExpiredJwtException expiredException){
        if (!publicRoutesMatcher.matches(request)) {
          ((HttpServletResponse) servletResponse).setStatus(SC_UNAUTHORIZED);
          return;
        }
      }catch (MalformedJwtException | UnsupportedJwtException | SignatureException exception) {
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        httpServletResponse.setStatus(SC_UNAUTHORIZED);
        httpServletResponse.getWriter().print(exception.getMessage());
        return;
      }
    }
    filterChain.doFilter(request, servletResponse);
  }

  @Override
  public void destroy() {

  }
}
