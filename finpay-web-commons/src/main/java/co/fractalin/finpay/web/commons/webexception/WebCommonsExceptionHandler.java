package co.fractalin.finpay.web.commons.webexception;

import com.google.common.collect.ImmutableMap;
import co.fractalin.finpay.commons.dto.error.ErrorMessage;
import co.fractalin.finpay.commons.exception.ForbiddenActionException;
import co.fractalin.finpay.commons.exception.FunctionalException;
import co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Objects;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.*;
import static org.springframework.http.HttpStatus.*;

/**
 * Gestionnaire d'erreur d'API FINPAY
 */
@Slf4j
@ControllerAdvice
public class WebCommonsExceptionHandler {

  private static final String maxFileSize = "2MB";

  private static final ImmutableMap<FunctionalErrorCode, HttpStatus> STATUS_MAP = ImmutableMap.<FunctionalErrorCode, HttpStatus>builder()
      .put(NOT_NULL_FIELDS, BAD_REQUEST)
      .put(INCORRECT_LENGTH_FIELDS, BAD_REQUEST)
      .put(NO_BLANK_FIELDS, BAD_REQUEST)
      .put(FORBIDDEN_ACTION, FORBIDDEN)
      .put(DIACTIVATED_ENTITY, UNAVAILABLE_FOR_LEGAL_REASONS)
      .put(UNKNOWN_ENTITY, NOT_FOUND)
      .build();

  @ExceptionHandler(FunctionalException.class)
  public ResponseEntity<ErrorMessage> handleFunctionalException(FunctionalException exception) {
    log.warn("Exception fonctionnelle :", exception);
    return new ResponseEntity<>(exception.toErrorMessage(), STATUS_MAP.getOrDefault(exception.getErrorCode(), BAD_REQUEST));
  }

  @ResponseBody
  @ResponseStatus(FORBIDDEN)
  @ExceptionHandler(AccessDeniedException.class)
  public ErrorMessage handleAccessDeniedException() {
    return new ForbiddenActionException().toErrorMessage();
  }

/*
  @ResponseBody
  @ResponseStatus(BAD_REQUEST)
  @ExceptionHandler({
      MissingServletRequestParameterException.class,
      HttpMessageNotReadableException.class,
      MethodArgumentTypeMismatchException.class,
      MultipartException.class,
      PropertyReferenceException.class})
  public ErrorMessage handleMalformedRequestException(Exception exception) {
    log.warn("Exception :", exception);
    FunctionalException functionalException = isCausedBy(exception, FileSizeLimitExceededException.class) ?
        new SizeLimitExceededException(maxFileSize) :
        new MalformedRequestException();

    return functionalException.toErrorMessage();
  }
*/

  private static boolean isCausedBy(Throwable exception, Class<? extends Throwable> causeType) {
    Throwable cause = exception.getCause();

    while (cause != null && !Objects.equals(cause, exception)) {
      if (causeType.isInstance(cause)) {
        return true;
      }
      cause = cause.getCause();
    }
    return false;
  }

}
