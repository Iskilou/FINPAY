package co.fractalin.finpay.web.commons.swagger;

import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;

import javax.validation.constraints.NotNull;

import java.util.Optional;

import static springfox.documentation.swagger.common.SwaggerPluginSupport.pluginDoesApply;

/**
 * Exemple de plugin Swagger pour affiner la définition des modèles à afficher dans swagger-ui
 */
@Order
@Component
public class CustomModelPropertyBuilder implements ModelPropertyBuilderPlugin {
  @Override
  public void apply(ModelPropertyContext modelPropertyContext) {
    //TODO Prendre en compte les autres annotations
    modelPropertyContext.getBuilder().required(Optional.of(modelPropertyContext.getBeanPropertyDefinition().get())
        .map(BeanPropertyDefinition::getField)
        .map(field -> field.hasAnnotation(NotNull.class))
        .orElse(false));
  }

  @Override
  public boolean supports(DocumentationType documentationType) {
    return pluginDoesApply(documentationType);
  }
}
