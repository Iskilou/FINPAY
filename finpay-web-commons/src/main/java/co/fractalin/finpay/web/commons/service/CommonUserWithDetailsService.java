package co.fractalin.finpay.web.commons.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Service associé à la recherche des utilisteurs
 */
public interface CommonUserWithDetailsService  {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
