package co.fractalin.finpay.web.commons.security;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

/**
 * Matcher pour les routes publiques accessible sans authentification
 */
public class PublicRoutesMatcher implements RequestMatcher {

  protected static final String GET = "GET";
  protected static final String POST = "POST";
  protected static final String PUT = "PUT";

  private static final List<RequestMatcher> COMMON_MATCHERS = Stream.of("swagger", "swagger-resources/configuration/", "v2/api-docs", "webjars/", "images/")
      .map(route -> '/' + route + "**")
      .map(AntPathRequestMatcher::new)
      .collect(toList());

  private final RequestMatcher delegate;

  public PublicRoutesMatcher(RequestMatcher... additionalMatchers) {
    List<RequestMatcher> matchers = new ArrayList<>(COMMON_MATCHERS);
    matchers.addAll(asList(additionalMatchers));

    delegate = new OrRequestMatcher(matchers);
  }

  @Override
  public boolean matches(HttpServletRequest request) {
    return delegate.matches(request);
  }
}
