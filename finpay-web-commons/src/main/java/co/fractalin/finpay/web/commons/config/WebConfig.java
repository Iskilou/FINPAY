package co.fractalin.finpay.web.commons.config;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import co.fractalin.finpay.commons.utils.JacksonUtils;
import co.fractalin.finpay.web.commons.security.AuthenticationTokenFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

import static com.fasterxml.jackson.databind.MapperFeature.DEFAULT_VIEW_INCLUSION;
import static co.fractalin.finpay.web.commons.config.WebConfig.SECURITY_PACKAGE;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Configuration des pages et JsonView
 */
@ComponentScan(basePackages = {SECURITY_PACKAGE})
public abstract class WebConfig implements WebMvcConfigurer {

  private static final int DEFAULT_PAGE_SIZE = 20;
  private static final int MAX_PAGE_SIZE = 500;
  public static final String SECURITY_PACKAGE = "co.fractalin.finpay.web.commons.security";

  protected static ObjectMapper mapper() {
    ObjectMapper mapper = JacksonUtils.mapper();

    mapper.registerModule(new PageResponseModule());
    mapper.disable(DEFAULT_VIEW_INCLUSION);

    return mapper;
  }

  @Override
  public void configurePathMatch(PathMatchConfigurer configurer) {
    configurer.setUseSuffixPatternMatch(false);
  }

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer
        .defaultContentType(APPLICATION_JSON)
        .favorPathExtension(false)
        .ignoreAcceptHeader(false);
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
    resolver.setFallbackPageable(PageRequest.of(0, DEFAULT_PAGE_SIZE, Sort.unsorted()));
    resolver.setOneIndexedParameters(true);
    resolver.setMaxPageSize(MAX_PAGE_SIZE);

    argumentResolvers.add(resolver);
  }

  @Bean
  public FilterRegistrationBean<AuthenticationTokenFilter<?>> registration(AuthenticationTokenFilter<?> tokenFilter) {
    FilterRegistrationBean<AuthenticationTokenFilter<?>> registration = new FilterRegistrationBean<>(tokenFilter);
    registration.setEnabled(false);
    return registration;
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }


  /**
   * Permet d'utiliser les {@link JsonView} avec les {@link Page}
   */
  private static class PageResponseModule extends SimpleModule {

    @Override
    public void setupModule(SetupContext context) {
      context.setMixInAnnotations(Page.class, PageResponse.class);
      addAbstractTypeMapping(Page.class, PageResponse.class);
    }
  }
}
