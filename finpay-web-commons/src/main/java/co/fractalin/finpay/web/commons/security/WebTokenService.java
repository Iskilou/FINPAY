package co.fractalin.finpay.web.commons.security;

import io.jsonwebtoken.Jwts;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static co.fractalin.finpay.model.utils.LambdaUtils.by;
import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static java.lang.Math.toIntExact;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Date.from;
import static java.util.regex.Pattern.compile;

/**
 * Classe de manipulation des tokens
 */
@RequiredArgsConstructor
public abstract class WebTokenService<T extends ParsedToken, U extends UserDetails> {

  protected static final String INSUFFICIENT_AUTHENTICATION_EXCEPTION_MESSAGE = "Le token ne peut pas être raffraîchi";
  public static final String COOKIE_NAME = "token";

  @Getter
  private final String secret;

  private final long expirationDuration;

  private final String authHeader;

  private final String authPrefix;

  @Getter
  private final ConversionService conversionService = new DefaultConversionService();

  private Pattern tokenExtractor;

  @PostConstruct
  public void init() {
    tokenExtractor = compile("(?:" + authPrefix + ")?\\s*(\\S+)\\s*$");
  }

  public abstract String refresh(T parsedToken, U user);

  public abstract T parse(String token);

  public String getToken(HttpServletRequest request) {
    return Optional.ofNullable(request.getHeader(authHeader))
        .map(tokenExtractor::matcher)
        .filter(Matcher::matches)
        .map(matcher -> matcher.group(1))
        .orElseGet(() -> getToken(request.getCookies()));
  }

  private static String getToken(Cookie[] cookies) {
    return cookies == null ? null : Stream.of(cookies)
        .filter(by(Cookie::getName, COOKIE_NAME))
        .map(Cookie::getValue)
        .findFirst()
        .orElse(null);
  }

  public void addTokenToResponse(HttpServletResponse response, String token) {
    response.addCookie(getCookie(token));
    response.addHeader(authHeader, authPrefix + ' ' + token);
  }

  private Cookie getCookie(String token) {
    return getCookie(token, toIntExact(Duration.ofMinutes(expirationDuration).get(SECONDS)));
  }

  private static Cookie getCookie(String token, int maxAge) {
    Cookie cookie = new Cookie(COOKIE_NAME, token);
    cookie.setHttpOnly(true);
    cookie.setMaxAge(maxAge);
    cookie.setPath("/");
    return cookie;
  }

  public static void deleteCookie(HttpServletResponse response) {
    // Le seul moyen de supprimer un cookie est de le redéfinir avec une durée de vie égale à 0
    response.addCookie(getCookie(null, 0));
  }

  protected String generateTokenFromClaims(Map<String, Object> claims) {
    return Jwts.builder()
        .setClaims(new HashMap<>(claims))
        .setExpiration(getExpirationDate())
        .signWith(HS256, secret)
        .compact();
  }

  private Date getExpirationDate() {
    return from(now().plus(expirationDuration, MINUTES));
  }

  public abstract Authentication getAuthentication(T token);
}
