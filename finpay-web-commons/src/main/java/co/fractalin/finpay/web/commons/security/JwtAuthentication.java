package co.fractalin.finpay.web.commons.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

import static lombok.AccessLevel.PROTECTED;

/**
 * Objet qui représente le token d'authentification simplifié
 */
@RequiredArgsConstructor
public class JwtAuthentication<T extends ParsedToken> implements Authentication {

  @Getter(PROTECTED)
  private final T token;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getDetails() {
    return token;
  }

  @Override
  public Object getPrincipal() {
    return token;
  }

  @Override
  public boolean isAuthenticated() {
    return token != null;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    throw new IllegalArgumentException("setAuthenticated n'est pas supporté");
  }

  @Override
  public String getName() {
    return token.getUsername();
  }
}
