package co.fractalin.finpay.discovery.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final String role;

  public SecurityConfig(@Value("${eureka.role}") String role) {
    this.role = role;
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth,
                              @Value("${eureka.user}") String user,
                              @Value("${eureka.password}") String password) throws Exception {

    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    auth.inMemoryAuthentication()
        .withUser(user).password(encoder.encode(password)).roles(role, "ADMIN");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        .and().requestMatchers().antMatchers("/eureka/**")
        .and().authorizeRequests().antMatchers("/eureka/**")
        .hasRole(role).anyRequest().denyAll().and()
        .httpBasic().and().csrf().disable();
  }
}
