package co.fractalin.finpay.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class FinpayDiscoveryApplication {

  public static void main(String[] args) {
    SpringApplication.run(FinpayDiscoveryApplication.class, args);
  }
}
