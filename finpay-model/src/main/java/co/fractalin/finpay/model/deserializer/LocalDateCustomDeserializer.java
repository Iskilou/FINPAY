package co.fractalin.finpay.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDate;

public class LocalDateCustomDeserializer extends StdDeserializer<LocalDate> {

  protected LocalDateCustomDeserializer() {
    super(LocalDate.class);
  }

  @Override
  public LocalDate deserialize(JsonParser parser, DeserializationContext context) throws IOException {
    ObjectCodec oc = parser.getCodec();
    JsonNode node = oc.readTree(parser);
    return LocalDate.of(node.get("year").asInt(), node.get("month").asInt(), node.get("day").asInt());
  }
}
