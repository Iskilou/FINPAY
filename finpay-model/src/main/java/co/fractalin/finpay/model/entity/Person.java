package co.fractalin.finpay.model.entity;

import co.fractalin.finpay.model.deserializer.LocalDateCustomDeserializer;
import co.fractalin.finpay.model.enumeration.Gender;
import co.fractalin.finpay.model.groups.Create;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

import static lombok.AccessLevel.PROTECTED;

/**
* Classe regroupant les champs des personnes physiques
*/
@Data
@NoArgsConstructor
@AllArgsConstructor(access = PROTECTED)
public abstract class Person implements Serializable {

  public static final int LASTNAME_MAX_LENGTH = 100;
  public static final int FIRSTNAME_MAX_LENGTH = 100;

  private String id;

  @NotBlank(groups = Create.class)
  @Length(max = FIRSTNAME_MAX_LENGTH)
  private String firstName;

  @NotBlank(groups = Create.class)
  @Length(max = LASTNAME_MAX_LENGTH)
  private String lastName;

  @JsonProperty("birth_date")
  @NotNull(groups = Create.class)
  @JsonDeserialize(using = LocalDateCustomDeserializer.class)
  private LocalDate birthDate;
  private Gender gender;

  @NotNull
  private String phoneNumber;


  private String zipCode;

  private String street;

  private String cityId;

  @NotNull
  @JsonProperty("user_id")
  private String userId;
}