package co.fractalin.finpay.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan
public interface FinpayModelConfig {

}
