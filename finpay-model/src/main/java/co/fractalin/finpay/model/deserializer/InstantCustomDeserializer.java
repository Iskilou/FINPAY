package co.fractalin.finpay.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.time.Instant;

public class InstantCustomDeserializer extends JsonDeserializer<Instant> {

  @Override
  public Instant deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
    ObjectCodec oc = parser.getCodec();
    JsonNode node = oc.readTree(parser);
    return Instant.ofEpochSecond(node.get("seconds").asLong(), node.get("nanos").asLong());
  }
}
