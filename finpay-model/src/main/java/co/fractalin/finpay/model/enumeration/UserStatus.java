package co.fractalin.finpay.model.enumeration;

/**
 * Statut d'un compte utilisateur
 */
public enum UserStatus {
  TO_BE_ACTIVATED, ACTIVATED, INACTIVE;

  public static final int USER_STATUS_MAX_LENGTH = 11;
}
