package co.fractalin.finpay.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Entité indiquant les personnes morales
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Organization {

  private String id;
  private String name;
  private String address;
  private String Country;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
