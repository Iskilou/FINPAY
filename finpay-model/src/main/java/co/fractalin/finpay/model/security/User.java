package co.fractalin.finpay.model.security;

import co.fractalin.finpay.model.enumeration.Role;
import co.fractalin.finpay.model.enumeration.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
* Utilisateur qui pourra se connecter à FINPAY
*/
@Document("User")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
  @Id
  private String id;
  private String username;
  private String password;
  private String passwordHash;
  private UserStatus status;
  private Role role ;
  private String organizationId;


}