package co.fractalin.finpay.model.exception;

import static java.lang.String.format;

/**
 * Cette exception sera levée si un champ (type) est inconnu du système
 */
public class UnknownMessageFieldException extends RuntimeException {
  public static final String UNKNOWN_MESSAGE_FIELD = "Champ de message %s inconnu";

  public UnknownMessageFieldException(String field) {
    super(format(UNKNOWN_MESSAGE_FIELD, field));
  }
}
