package co.fractalin.finpay.model.utils;

import lombok.NoArgsConstructor;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;

import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Collection de méthodes utilitaires pour valider des éléments tels que email et numéro de téléphone.
 */
@NoArgsConstructor(access = PRIVATE)
public final class ValidationUtils {

    public static final int SECRET_CODE_MAX_LENGTH = 4;
    public static final String SECRET_CODE_REGEXP = "^[0-9]{4}$";
    public static final String NUMBER_REGEXP = "\\d+";

    public static final int PASSWORD_MAX_LENGTH = 28;
    public static final String PASSWORD_REGEXP = "^(?=.*?[A-Z])" // contient au moins une lettre majuscule
            + "(?=.*?[a-z])" // contient au moins une lettre minuscule
            + "(?=.*?[0-9])" // contient au moins un caractère numérique
            + "(?=.*?[€£§@#$<,{;)%&*:>.!?+_=|'\\[~\"(}/\\]\\-\\^\\\\])" // contient au moins un caractère spécial
            + "(?!.*\\s)" // ne contient pas d'espace
            + "(?!.*[ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿŽŠÆÐÑØÞßæøñþ])" // ne contient pas un caractère accentué
            + ".{8,28}$";

    public static final int EMAIL_MAX_LENGTH = 255;

    private static final EmailValidator VALIDATOR = new EmailValidator();

    public static boolean isValidEmail(String email) {
        return isNotBlank(email) && email.length() <= EMAIL_MAX_LENGTH && VALIDATOR.isValid(email, null);
    }
}
