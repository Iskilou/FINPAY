package co.fractalin.finpay.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import java.time.ZonedDateTime;
import java.util.UUID;

import static java.time.ZonedDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.UUID.randomUUID;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserToken {

  private static final int VALUE_MAX_LENGTH = 255;

  private String id;

  private String value;

  private ZonedDateTime date;

  private String userId;

  public UserToken(String userId) {
    setDate(now());
    setValue(generateValue(userId));
    this.userId = userId;
  }

  protected static String generateValue(String prefixValue) {
    return UUID
            .nameUUIDFromBytes(prefixValue
                .toString()
                .concat(randomUUID().toString())
                .getBytes())
            .toString();
  }

  @JsonIgnore
  public boolean isValid(long tokenDuration) {
    ZonedDateTime now = now();
    return now.isAfter(date) && SECONDS.between(date, now) < tokenDuration;
  }
}
