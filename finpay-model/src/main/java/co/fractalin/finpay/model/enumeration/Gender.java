package co.fractalin.finpay.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Gender {
  MALE,
  FEMALE,
  OTHER;

  private final String personGender;

  public static final int PERSON_GENDER_LENGTH = 6;

  Gender() {
    this(null);
  }
}
