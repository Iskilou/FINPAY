package co.fractalin.finpay.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.nio.ByteBuffer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotNull;

/**
 * Entités représentant les médias
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Media {

    private String id;

    @JsonProperty("original_name")
    @NotNull
    private String originalName;

    @JsonProperty("internal_name")
    @NotNull
    private String internalName;

    private String type;
    private ByteBuffer content;
}
