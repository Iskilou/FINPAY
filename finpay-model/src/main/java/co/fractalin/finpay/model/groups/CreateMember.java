package co.fractalin.finpay.model.groups;

/**
 * Marqueur indiquant qu'une validation doit être effectuée pendant la phase de création d'un membre
 */
public interface CreateMember {
}
