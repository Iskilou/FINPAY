package co.fractalin.finpay.model.enumeration;

import lombok.RequiredArgsConstructor;
/**
 * Statut d'un portefeuille
 */
@RequiredArgsConstructor
public enum AccountStatus {
  TO_BE_ACTIVATED,
  ACTIVATE,
  DEACTIVATE;
  public static final int ACCOUNT_STATUS_MAX_LENGTH = 11;

}
