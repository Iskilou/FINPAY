package co.fractalin.finpay.model.entity;

import co.fractalin.finpay.model.enumeration.AccountStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@Document("account")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Account {

  @Id
  private String id;

  private String label;

  private String code;

  private String description;

  private String userId;

  private AccountStatus status;

  private BigDecimal amount ;

  private Date creationDate;

  private Date modificationDate;

}
