package co.fractalin.finpay.model.constants;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class EntityConstants {
  public static final int BIG_DECIMAL_PRECISION = 10;
  public static final int BIG_DECIMAL_SCALE = 2;
}
