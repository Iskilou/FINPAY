package co.fractalin.finpay.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

/**
* Type de rôle
*/
@Getter
@RequiredArgsConstructor
public enum Role {
  SUPER_ADMIN(true, true, true,"Super Administrateur FINPAY"),
  BASIC(false, false, false, "Utilisateur  forfait Basic "),
  PREMIUM(false, false, false, "Utilisateur Premium"),
  ADMIN(false, false, true, "Administrateur de FINPAY");

  private final boolean admin;
  private final boolean authorizedForAllServices;
  private final boolean authorizedForAllServicesInParticularPCN;
  private final String label;

  public static final int ROLE_LENTGH = 16;

  public boolean hasFeatures(Feature... features) {
    return Stream.of(features)
        .map(Feature::getRoles)
        .allMatch(roles -> roles.contains(this));
  }
}