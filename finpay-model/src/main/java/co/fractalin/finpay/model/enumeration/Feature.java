package co.fractalin.finpay.model.enumeration;

import lombok.Getter;

import java.util.EnumSet;
import java.util.Set;

import static com.google.common.collect.ImmutableSet.copyOf;

/**
 * Liste des fonctionnalités disponibles pour chaque role
 */
@Getter
public enum Feature {

  /* FundManagers*/
  FIND_ALL_FUND_MANAGERS(Role.SUPER_ADMIN, Role.ADMIN),
  FIND_FUND_MANAGER(Role.SUPER_ADMIN, Role.ADMIN ),
  ADD_FUND_MANAGER(Role.SUPER_ADMIN, Role.ADMIN),
  UPDATE_FUND_MANAGER(Role.SUPER_ADMIN, Role.ADMIN),

  /* Account*/
  FIND_ALL_ACCOUNTS(Role.SUPER_ADMIN, Role.ADMIN),
  FIND_ACCOUNT(Role.SUPER_ADMIN, Role.ADMIN ),
  CREATE_FUND_MANAGER(Role.SUPER_ADMIN, Role.ADMIN),
  UPDATE_FUND_ACCOUNT(Role.SUPER_ADMIN, Role.ADMIN),


  /*Medias*/
  FIND_MEDIA(Role.SUPER_ADMIN, Role.ADMIN),
  CREATE_MEDIA(Role.SUPER_ADMIN, Role.ADMIN);


  private final Set<Role> roles;

  Feature(Role... roles) {
    this.roles = copyOf(EnumSet.of(Role.SUPER_ADMIN, roles));
  }
}
