package co.fractalin.finpay.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
* Gestionnaire de fonds
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("fundManager")
public class FundManager {

  @Id
  private String id;

  @NotNull
  @NotBlank
  private String name;

  @NotNull
  @NotBlank
  @JsonProperty("account_id")
  private String accountId;
}







