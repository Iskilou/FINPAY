package co.fractalin.finpay.model.utils;

import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import static lombok.AccessLevel.PRIVATE;

/**
 * Collection de méthodes utilitaires visant à faciliter l'utilisations des lambdas et références de méthodes
 */
@NoArgsConstructor(access = PRIVATE)
public class LambdaUtils {

  public static <T, F> Predicate<T> byPredicate(Function<T, F> getter, Predicate<F> predicate) {
    return element -> predicate.test(getter.apply(element));
  }

  public static <T> Predicate<T> not(Predicate<T> predicate) {
    return predicate.negate();
  }

  /**
   * Permet de filtrer un stream en fonction de la valeur d'un champ des élément du stream.
   * <p>
   * Exemple: {@code bikes.stream().filter(by(VlsBike::getVlsStatus, AVAILABLE))}
   *
   * @param getter accesseur permettant d'obtenir la valeur du champ à comparer
   * @param value  valeur à laquelle le champ doit être égal
   * @param <T>    Type de l'objet filtré
   * @param <F>    Type du champ
   * @return prédicat retournant {@code true} si le résultat de l'application du getter sur son paramètre est égale à {@code value}
   */
  public static <T, F> Predicate<T> by(Function<T, F> getter, F value) {
    return element -> Objects.equals(getter.apply(element), value);
  }

  /**
   * Renvoie toujours la valeur de la fonction donnée.
   *
   * @param value la valeur de la fonction en sortie
   * @param <I>   Type de la fonction en entrée
   * @param <O>   Type de la fonction en sortie
   * @return la valeur de la function en sortie, la valeur de la fonction en entrée est ignorée
   */
  public static <I, O> Function<I, O> always(O value) {
    return ignored -> value;
  }
}
