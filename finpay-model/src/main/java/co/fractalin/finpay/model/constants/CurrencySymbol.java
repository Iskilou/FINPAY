package co.fractalin.finpay.model.constants;

import java.util.HashMap;


public class CurrencySymbol {

  private final HashMap<String, String> symbol = new HashMap<>();

  public CurrencySymbol() {
    this.symbol.put("USD", "$");
    this.symbol.put("CAD", "CA$");
    this.symbol.put("EUR", "€");
    this.symbol.put("AED", "AED");
    this.symbol.put("AFN", "Af");
    this.symbol.put("ALL", "ALL");
    this.symbol.put("AMD", "AMD");
    this.symbol.put("ARS", "AR$");
    this.symbol.put("AUD", "AU$");
    this.symbol.put("AZN", "man.");
    this.symbol.put("BAM", "KM");
    this.symbol.put("BDT", "Tk");
    this.symbol.put("BGN", "BGN");
    this.symbol.put("BHD", "BD");
    this.symbol.put("BIF", "FBu");
    this.symbol.put("BND", "BN$");
    this.symbol.put("BOB", "Bs");
    this.symbol.put("BRL", "R$");
    this.symbol.put("BWP", "BWP");
    this.symbol.put("BYR", "BYR");
    this.symbol.put("BZD", "BZ$");
    this.symbol.put("CDF", "CDF");
    this.symbol.put("CHF", "CHF");
    this.symbol.put("CLP", "CL$");
    this.symbol.put("CNY", "CN¥");
    this.symbol.put("COP", "CO$");
    this.symbol.put("CRC", "₡");
    this.symbol.put("CVE", "CV$");
    this.symbol.put("CZK", "Kč");
    this.symbol.put("DJF", "Fdj");
    this.symbol.put("DKK", "Dkr");
    this.symbol.put("DOP", "RD$");
    this.symbol.put("DZD", "DA");
    this.symbol.put("EEK", "Ekr");
    this.symbol.put("EGP", "EGP");
    this.symbol.put("ERN", "Nfk");
    this.symbol.put("ETB", "Br");
    this.symbol.put("GBP", "£");
    this.symbol.put("GEL", "GEL");
    this.symbol.put("GHS", "GH₵");
    this.symbol.put("GNF", "FG");
    this.symbol.put("GTQ", "GTQ");
    this.symbol.put("HKD", "HK$");
    this.symbol.put("HNL", "HNL");
    this.symbol.put("HRK", "kn");
    this.symbol.put("HUF", "Ft");
    this.symbol.put("IDR", "Rp");
    this.symbol.put("ILS", "₪");
    this.symbol.put("INR", "Rs");
    this.symbol.put("IQD", "IQD");
    this.symbol.put("IRR", "IRR");
    this.symbol.put("ISK", "Ikr");
    this.symbol.put("JMD", "J$");
    this.symbol.put("JOD", "JD");
    this.symbol.put("JPY", "¥");
    this.symbol.put("KES", "Ksh");
    this.symbol.put("KHR", "KHR");
    this.symbol.put("KMF", "CF");
    this.symbol.put("KRW", "₩");
    this.symbol.put("KWD", "KD");
    this.symbol.put("KZT", "KZT");
    this.symbol.put("LBP", "LB£");
    this.symbol.put("LKR", "SLRs");
    this.symbol.put("LTL", "Lt");
    this.symbol.put("LVL", "Ls");
    this.symbol.put("LYD", "LD");
    this.symbol.put("MAD", "MAD");
    this.symbol.put("MDL", "MDL");
    this.symbol.put("MGA", "MGA");
    this.symbol.put("MKD", "MKD");
    this.symbol.put("MMK", "MMK");
    this.symbol.put("MOP", "MOP$");
    this.symbol.put("MUR", "MURs");
    this.symbol.put("MXN", "MX$");
    this.symbol.put("MYR", "RM");
    this.symbol.put("MZN", "MTn");
    this.symbol.put("NAD", "N$");
    this.symbol.put("NGN", "₦");
    this.symbol.put("NIO", "C$");
    this.symbol.put("NOK", "Nkr");
    this.symbol.put("NPR", "NPRs");
    this.symbol.put("NZD", "NZ$");
    this.symbol.put("OMR", "OMR");
    this.symbol.put("PAB", "B/.");
    this.symbol.put("PEN", "S/.");
    this.symbol.put("PHP", "₱");
    this.symbol.put("PKR", "PKRs");
    this.symbol.put("PLN", "zł");
    this.symbol.put("PYG", "₲");
    this.symbol.put("QAR", "QR");
    this.symbol.put("RON", "RON");
    this.symbol.put("RSD", "din.");
    this.symbol.put("RUB", "RUB");
    this.symbol.put("RWF", "RWF");
    this.symbol.put("SAR", "SR");
    this.symbol.put("SDG", "SDG");
    this.symbol.put("SEK", "Skr");
    this.symbol.put("SGD", "S$");
    this.symbol.put("SOS", "Ssh");
    this.symbol.put("SYP", "SY£");
    this.symbol.put("THB", "฿");
    this.symbol.put("TND", "DT");
    this.symbol.put("TOP", "T$");
    this.symbol.put("TRY", "TL");
    this.symbol.put("TTD", "TT$");
    this.symbol.put("TWD", "NT$");
    this.symbol.put("TZS", "TSh");
    this.symbol.put("UAH", "₴");
    this.symbol.put("UGX", "USh");
    this.symbol.put("UYU", "$U");
    this.symbol.put("UZS", "UZS");
    this.symbol.put("VEF", "Bs.F.");
    this.symbol.put("VND", "₫");
    this.symbol.put("XAF", "FCFA");
    this.symbol.put("XOF", "CFA");
    this.symbol.put("YER", "YR");
    this.symbol.put("ZAR", "R");
    this.symbol.put("ZMK", "ZK");
  }

  public String getSymbol(String currency) {
    return this.symbol.get(currency);
  }
}
