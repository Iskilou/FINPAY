package co.fractalin.finpay.model.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.stream.Stream;

import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;

public class LambdaUtilsTest {
  @Test
  public void shouldFilterBy() {
    TestEntity entity = new TestEntity("Foo Bar");

    assertThat(Stream.of(entity, new TestEntity("Wonder Woman"))
        .filter(LambdaUtils.by(TestEntity::getName, entity.getName())).findFirst()).contains(entity);
  }

  @Test
  public void shouldAlways() {
    TestEntity entity = new TestEntity("test");
    Assertions.assertThat(LambdaUtils.always(entity).apply(now())).isEqualTo(entity);
  }

  @Getter
  @RequiredArgsConstructor
  private static class TestEntity {
    private final String name;
  }
}