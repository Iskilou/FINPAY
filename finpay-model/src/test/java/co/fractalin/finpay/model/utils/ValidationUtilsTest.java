package co.fractalin.finpay.model.utils;

import org.assertj.core.api.Assertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.assertj.core.api.Assertions.assertThat;

public class ValidationUtilsTest {

  private static final Pattern PASSWORD_PATTERN = Pattern.compile(ValidationUtils.PASSWORD_REGEXP);
  private static final String VALID_EMAIL_PROVIDER = "validEmailProvider";
  private static final String INVALID_EMAIL_PROVIDER = "invalidEmailProvider";
  private static final String VALID_PASSWORD_PROVIDER = "validPasswordProvider";
  private static final String INVALID_PASSWORD_PROVIDER = "invalidPasswordProvider";

  @DataProvider(name = VALID_EMAIL_PROVIDER)
  public Object[][] validEmailProvider() {
    return new Object[][] {
        { "user@1.com" },
        { "user.name@isci.inc.com" },
        { "user-name@isci-inc.com" },
        { "user+name@isci.ca" },
        { "user@isci.com" },
        { "user%name@isci.com" },
        { "user_name@isci.com" },
        { "user123@isci.com" },
        { "user.123.456@isci.com" },
        { "USER@isci.com" },
        { "USER@ISCI.inc.uk" },
        { "user@isci+inc.com" },
        { "user@isci?inc.com" },
        { "user@isci.comfr" },
        { "user@isci.c" },
        { "user@isci" },
        { "user@%*.com" },
        { "user_" + repeat('a', 19) + "@isci_" + repeat('a', 216) + ".com" }

    };
  }

  @Test(dataProvider = VALID_EMAIL_PROVIDER)
  public void shouldAcceptValidEmail(String email) {
    Assertions.assertThat(ValidationUtils.isValidEmail(email)).isTrue();
  }

  @DataProvider(name = INVALID_EMAIL_PROVIDER)
  public Object[][] invalidEmailProvider() {
    return new Object[][] {
        { null },
        { "" },
        { " " },
        { " user@isci.com" },
        { "user" },
        { "user@" },
        { "user@.com" },
        { "user.@isci.com" },
        { "@isci.com" },
        { "user@name@isci.com" },
        { "email_with_local_too_long_" + repeat('a', 39) + "@isci.com" },
        { "email_with_domain_too_long@" + repeat('a', 252) + ".com" },
        { "email_too_long" + repeat('a', 20) + "@" + repeat('a', 217) + ".com" }
    };
  }

  @Test(dataProvider = INVALID_EMAIL_PROVIDER)
  public void shouldRejectInvalidEmail(String email) {
    Assertions.assertThat(ValidationUtils.isValidEmail(email)).isFalse();
  }

  @DataProvider(name = VALID_PASSWORD_PROVIDER)
  public Object[][] validPasswordProvider() {
    return new Object[][] {
        { "z+00" + repeat('Z', 24) },
        { "@aA44444" },
        { "'bB55555" },
        { "!cC00000" },
        { "#dD11111" },
        { "$eE22222" },
        { "<fF33333" },
        { ",gG44444" },
        { "{hH55555" },
        { ";iI66666" },
        { "(jJ77777" },
        { "%kK88888" },
        { "^lL99999" },
        { "&mM00000" },
        { "*nN11111" },
        { ")oO22222" },
        { "}pP33333" },
        { ":qQ44444" },
        { ">rR55555" },
        { ".sS66666" },
        { "?tT77777" },
        { "/uU88888" },
        { "\\vV99999" },
        { "+wW00000" },
        { "-xX11111" },
        { "_yY22222" },
        { "=zZ33333" },
        { "|aA44444" },
        { "]bB55555" },
        { "\"cC66666" },
        { "[dD77777" },
        { "~eE88888" },
        { "999ffFF€" },
        { "000ggGG£" },
        { "111hhHH§" }
    };
  }

  @Test(dataProvider = VALID_PASSWORD_PROVIDER)
  public void shouldAcceptValidPassword(String password) {
    assertThat(isValidPassword(password)).isTrue();
  }

  @DataProvider(name = INVALID_PASSWORD_PROVIDER)
  public Object[][] invalidPasswordProvider() {
    return new Object[][] {
        { null },
        { "" },
        { repeat(' ', 10) },
        { "AAAAa0@" },
        { repeat('A', 26) + "a0@" },
        { "AAA Aa0@" },
        { "AAAAàa0@" },
        { "AAAAaa00" },
        { "AAAAaa!!" },
        { "AAAA00]]" },
        { "aaaa00^^" },
        { "ŽŠÆÐÑØÞßæøñþAaa00++" }
    };
  }

  @Test(dataProvider = INVALID_PASSWORD_PROVIDER)
  public void shouldRejectInvalidPassword(String password) {
    assertThat(isValidPassword(password)).isFalse();
  }

  private static boolean isValidPassword(String password) {
    return isNotBlank(password) && PASSWORD_PATTERN.matcher(password).matches();
  }
}