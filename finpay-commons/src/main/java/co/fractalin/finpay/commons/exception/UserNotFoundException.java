package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.UNKNOWN_USERNAME;

public class UserNotFoundException extends FunctionalException {
  public UserNotFoundException(String username) {
    super(UNKNOWN_USERNAME, username);
  }
}
