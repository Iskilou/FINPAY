package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.INVALID_TOKEN;

public class InvalidTokenException extends FunctionalException {

  public InvalidTokenException(String tokenValue) {
    super(INVALID_TOKEN, tokenValue);
  }

}
