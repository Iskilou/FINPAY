package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.MULTIPLE_USERS_FOUND_WITH_EMAIL;
import static java.lang.String.format;

public class MultipleUsersFoundWithEmailException extends FunctionalException {
  public static final String GENERIC_ERROR_MESSAGE = "Une erreur est survenue. Veuillez contacter votre service client";
  public static final String MULTIPLE_USERS_FOUND_WITH_EMAIL_ERROR_MESSAGE = "Plusieurs utilisateurs trouvés avec l'email %s";

  public MultipleUsersFoundWithEmailException() {
    super(MULTIPLE_USERS_FOUND_WITH_EMAIL, GENERIC_ERROR_MESSAGE);
  }

  public MultipleUsersFoundWithEmailException(String email) {
    super(MULTIPLE_USERS_FOUND_WITH_EMAIL, format(MULTIPLE_USERS_FOUND_WITH_EMAIL_ERROR_MESSAGE, email));
  }
}
