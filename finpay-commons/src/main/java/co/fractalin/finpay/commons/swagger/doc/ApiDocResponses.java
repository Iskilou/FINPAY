package co.fractalin.finpay.commons.swagger.doc;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PROTECTED;

/**
 * Classe utilitaire contenant les descriptions des réponses des contrôleurs
 * utilisées pour la documentation de l'API
 */
@NoArgsConstructor(access = PROTECTED)
public class ApiDocResponses {

  private static final String LIST_OF = "Liste des ";
  private static final String FILTERED_LIST = " possiblement filtrée";

  public static final String DETAIL_MEDIA = "Détail d'un média";
  public static final String ORGANIZATIONS = LIST_OF + "organisations " + FILTERED_LIST;
  public static final String DETAIL_ORGANIZATION = "Détail d'une organisation";

  public static final String FORBIDDEN_ACTION = "Action non autorisée";
  public static final String UNAUTHENTICATED_USER = "Utilisateur non authentifié";
  public static final String NOT_FOUND = "Non trouvé";
  public static final String BAD_REQUEST = "Requête mal formatée";

  public static final String AUTHENTICATION_TOKEN = "Token d'authentification";
  public static final String COMMAND_IN_PROGRESS = "La commande est prise en compte.";
}
