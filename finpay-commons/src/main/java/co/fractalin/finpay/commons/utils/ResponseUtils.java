
package co.fractalin.finpay.commons.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * Classe permettant de remplir un objet avec des attributs par défaut
 */
@Slf4j
public class ResponseUtils {

  public static void fillNullObjects(Object object) {
    Field[] fields = object.getClass().getDeclaredFields();
    for (Field field : fields) {
      try {
        field.setAccessible(true);
        if (field.get(object) != null) {
          continue;
        } else if (field.getType().equals(BigDecimal.class)) {
          field.set(object, BigDecimal.ZERO);
        } else if (field.getType().equals(Integer.class)) {
          field.set(object, 0);
        } else if (field.getType().equals(String.class)) {
          field.set(object, "");
        } else if (field.getType().equals(Boolean.class)) {
          field.set(object, false);
        } else if (field.getType().equals(Character.class)) {
          field.set(object, '\u0000');
        } else if (field.getType().equals(Byte.class)) {
          field.set(object, (byte) 0);
        } else if (field.getType().equals(Float.class)) {
          field.set(object, 0.0f);
        } else if (field.getType().equals(Double.class)) {
          field.set(object, 0.0d);
        } else if (field.getType().equals(Short.class)) {
          field.set(object, (short) 0);
        } else if (field.getType().equals(Long.class)) {
          field.set(object, 0L);
        } else if (field.getType().getDeclaredFields().length > 0) {
          for (Constructor<?> constructor : field.getClass().getConstructors()) {
            if (constructor.getParameterTypes().length == 0) {
              field.set(object, constructor.newInstance());
              fillNullObjects(field.get(object));
            }
          }
        }
      } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
        log.error("{}", e);
      }
    }
  }


}
