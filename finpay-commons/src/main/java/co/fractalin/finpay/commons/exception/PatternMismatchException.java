package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.PATTERN_FIELDS;

public class PatternMismatchException extends FunctionalException {
  public PatternMismatchException(String fields) {
    super(PATTERN_FIELDS, fields);
  }
}
