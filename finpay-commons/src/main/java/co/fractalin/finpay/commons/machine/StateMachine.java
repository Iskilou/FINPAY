package co.fractalin.finpay.commons.machine;

import com.google.common.collect.ImmutableMap;
import co.fractalin.finpay.commons.exception.IllegalStatusChangeException;
import lombok.RequiredArgsConstructor;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static lombok.AccessLevel.PRIVATE;

/**
 * Machine à état permettant de vérifier qu'une transition d'un état à un autre est valide et de l'appliquer
 */
@RequiredArgsConstructor(access = PRIVATE)
public final class StateMachine<E, S extends Enum<S>> {
  @RequiredArgsConstructor(access = PRIVATE)
  public static final class Builder<T extends Enum<T>> {
    private final Map<T, Set<T>> transitions = new LinkedHashMap<>(1);

    public ToBuilder<T> from(T from) {
      return new ToBuilder<>(this, from);
    }

    @SafeVarargs
    private final void put(T from, T... to) {
      transitions.merge(from, new LinkedHashSet<>(asList(to)), (currentTos, newTos) -> {
        currentTos.addAll(newTos);
        return currentTos;
      });
    }

    public <E> StateMachine<E, T> build(Function<E, T> statusGetter, BiConsumer<E, T> statusSetter) {
      transitions.replaceAll((from, tos) -> EnumSet.copyOf(tos));
      return new StateMachine<>(ImmutableMap.copyOf(transitions), statusGetter, statusSetter);
    }
  }

  @RequiredArgsConstructor(access = PRIVATE)
  public static final class ToBuilder<T extends Enum<T>> {
    private final Builder<T> builder;
    private final T from;

    @SafeVarargs
    public final Builder<T> to(T... to) {
      builder.put(from, to);
      return builder;
    }
  }

  public static <T extends Enum<T>> Builder<T> builder() {
    return new Builder<>();
  }

  private final ImmutableMap<S, Set<S>> transitions;
  private final Function<E, S> statusGetter;
  private final BiConsumer<E, S> statusSetter;

  public boolean isTransitionLegal(S from, S to) {
    Set<S> nexts = transitions.get(from);

    return nexts != null && nexts.contains(to);
  }

  public E changeStatus(E entity, S newStatus) {
    S currentStatus = statusGetter.apply(entity);

    if (!isTransitionLegal(currentStatus, newStatus)) {
      throw new IllegalStatusChangeException(currentStatus, newStatus);
    }

    statusSetter.accept(entity, newStatus);

    return entity;
  }
}
