package co.fractalin.finpay.commons.exception;

import static java.lang.String.format;

public class IncorrectCurrencyException extends RuntimeException {
  public static final String UNKNOWN_MESSAGE_FIELD = "La monnaie du PCN (%s) est difference de celle fournie (%s).";

  public IncorrectCurrencyException(String pcnCurrency, String providedCurrency) {
    super(format(UNKNOWN_MESSAGE_FIELD, pcnCurrency, providedCurrency));
  }
}
