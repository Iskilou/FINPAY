package co.fractalin.finpay.commons.dto.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import static lombok.AccessLevel.PROTECTED;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = PROTECTED)
public abstract class AbstractEvent implements Serializable {

  private String type;
}
