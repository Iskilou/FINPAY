package co.fractalin.finpay.commons.dto.security;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.OLD_PASSWORD;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.PASSWORD;

/**
 * DTO utilisé par la requête de mise à jour du mot de passe d'un compte utilisateur (si l'ancien mot de passe est fourni)
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasswordUpdateRequest {
  @ApiModelProperty(OLD_PASSWORD)
  @NotBlank
  private String oldPassword;

  @ApiModelProperty(PASSWORD)
  @NotBlank
  private String newPassword;
}
