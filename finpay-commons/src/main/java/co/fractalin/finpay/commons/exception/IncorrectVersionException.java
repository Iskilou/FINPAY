package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.INCORRECT_VERSION;

public class IncorrectVersionException extends FunctionalException {
  public IncorrectVersionException() {
    super(INCORRECT_VERSION);
  }
}
