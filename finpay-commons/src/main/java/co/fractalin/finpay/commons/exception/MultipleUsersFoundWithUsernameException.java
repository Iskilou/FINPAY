package co.fractalin.finpay.commons.exception;

import java.util.UUID;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.MULTIPLE_USERS_FOUND_WITH_USERNAME;
import static java.lang.String.valueOf;

public class MultipleUsersFoundWithUsernameException extends FunctionalException {

  public MultipleUsersFoundWithUsernameException(String username) {
    super(MULTIPLE_USERS_FOUND_WITH_USERNAME, username);
  }
  public MultipleUsersFoundWithUsernameException(UUID id) {
    super(MULTIPLE_USERS_FOUND_WITH_USERNAME, valueOf(id));
  }
}
