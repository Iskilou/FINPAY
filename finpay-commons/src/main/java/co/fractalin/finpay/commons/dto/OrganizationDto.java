package co.fractalin.finpay.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationDto {
  private String id;
  private String name;
  private String countryId;
  private String userId;
}
