package co.fractalin.finpay.commons.mapper;

import co.fractalin.finpay.commons.dto.OrganizationDto;
import co.fractalin.finpay.model.entity.Organization;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrganizationMapper {
  OrganizationDto fromPcnOrganizationToPcnOrganizationDTO(Organization city);
  Organization fromPcnOrganizationDTOToPcnOrganization(OrganizationDto cityDTO);
}
