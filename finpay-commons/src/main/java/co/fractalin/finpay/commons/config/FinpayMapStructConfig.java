package co.fractalin.finpay.commons.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "co.fractalin.finpay.commons.mapper")
public class FinpayMapStructConfig {
}
