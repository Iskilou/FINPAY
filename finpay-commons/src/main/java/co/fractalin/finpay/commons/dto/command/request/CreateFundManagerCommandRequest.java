package co.fractalin.finpay.commons.dto.command.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import co.fractalin.finpay.commons.dto.request.CreateFundManagerRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * commande de création d'une institution financière
 */
@Getter
@Setter
@ToString
public class CreateFundManagerCommandRequest implements CommandRequest {

  private CreateFundManagerRequest createFundManagerRequest;

  public CreateFundManagerCommandRequest() {
  }

  @Builder
  public CreateFundManagerCommandRequest(CreateFundManagerRequest createFundManagerRequest) {
    this.createFundManagerRequest = createFundManagerRequest;
  }

  @Override
  public CreateFundManagerCommandRequest apply(String object, ObjectMapper mapper) {
    try {
      return mapper.readValue(object, CreateFundManagerCommandRequest.class);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
