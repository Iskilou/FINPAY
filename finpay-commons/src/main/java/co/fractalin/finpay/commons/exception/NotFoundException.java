package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.NOT_FOUND_ENTITY;

public class NotFoundException extends FunctionalException {
  public NotFoundException(Class<?> entityClass, String id) {
    super(NOT_FOUND_ENTITY, entityClass.getSimpleName(), id);
  }
}
