package co.fractalin.finpay.commons.swagger.doc;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

/**
 * Classe utilitaire contenant les descriptions des paramètres utilisées pour la documentation de l'API
 */
@NoArgsConstructor(access = PRIVATE)
public final class ApiDocConstants {

  public static final String ID = "Identifiant";
  public static final String AUTH_TOKEN = "Jeton d'identification";
  public static final String PASSWORD = "Mot de passe web";
  public static final String USERNAME = "Nom utilisateur";
  public static final String AUTHENTICATION_REQUEST = "Requête d'authentification'";
  public static final String OLD_PASSWORD = "Ancien mot de passe";
  public static final String EMAIL = "Email";
  public static final String TOKEN = "Token";
}
