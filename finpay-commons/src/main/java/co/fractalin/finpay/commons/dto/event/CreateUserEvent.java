package co.fractalin.finpay.commons.dto.event;

import co.fractalin.finpay.commons.dto.event.payload.UserEventPayload;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

import static co.fractalin.finpay.commons.dto.event.FinpayDomainEventType.USER_CREATION_EVENT;

/**
 * Classe de gestion des events pour la création des utilisateurs (eventsourcing)
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserEvent implements Serializable {
  private String type;
  private Map<String, Object> context;
  private UserEventPayload userEventPayload;

  @Builder
  public CreateUserEvent(Map<String, Object> context) {
    this.type= USER_CREATION_EVENT;
    this.context= context;
  }
}
