package co.fractalin.finpay.commons.utils;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.Clock.fixed;
import static java.time.Clock.systemDefaultZone;
import static java.time.LocalTime.MIDNIGHT;
import static java.time.ZoneId.systemDefault;
import static java.time.temporal.ChronoField.MILLI_OF_SECOND;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static lombok.AccessLevel.PRIVATE;

/**
 * Classe contenant des méthodes utilitaires pour les dates
 */
@NoArgsConstructor(access = PRIVATE)
public class DateUtils {
  public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
  public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy à HH:mm").withZone(systemDefault());
  public static final DateTimeFormatter FILENAME_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
  public static final DateTimeFormatter SIPS_FILENAME_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
  public static final DateTimeFormatter FILENAME_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
  private static final int HALF_SECOND = 500;

  public static ZonedDateTime toZonedDateTime(Date date) {
    return round(fixed(date.toInstant(), systemDefault()));
  }

  public static ZonedDateTime toZonedDateTime(LocalDate date) {
    return ZonedDateTime.of(date, MIDNIGHT, systemDefault());
  }

  public static LocalTime toLocalTime(ZonedDateTime date) {
    return date.withZoneSameInstant(systemDefault()).toLocalTime();
  }

  public static ImmutablePair<ZonedDateTime, ZonedDateTime> getPreviousDayInterval(ZonedDateTime date) {
    return ImmutablePair.of(date.minusDays(1).truncatedTo(DAYS), date.truncatedTo(DAYS));
  }

  public static ImmutablePair<ZonedDateTime, ZonedDateTime> getPreviousMonthInterval(ZonedDateTime date) {
    ZonedDateTime monthBeginDate = date.withDayOfMonth(1).truncatedTo(DAYS);
    return ImmutablePair.of(monthBeginDate.minusMonths(1), monthBeginDate);
  }

  public static String formatDate(Date date, DateTimeFormatter formatter) {
    return formatter.format(toZonedDateTime(date));
  }

  public static ZonedDateTime now() {
    return round(systemDefaultZone());
  }

  public static boolean isPast(ZonedDateTime date) {
    return date != null && date.isBefore(now());
  }

  public static boolean isFuture(ZonedDateTime date) {
    return date != null && date.isAfter(now());
  }

  static ZonedDateTime round(Clock clock) {
    ZonedDateTime now = ZonedDateTime.now(clock);

    return (now.get(MILLI_OF_SECOND) < HALF_SECOND ? now : now.plusSeconds(1)).truncatedTo(SECONDS);
  }

}
