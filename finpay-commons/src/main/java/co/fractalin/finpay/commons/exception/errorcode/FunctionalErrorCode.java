package co.fractalin.finpay.commons.exception.errorcode;

import lombok.Getter;

import static java.lang.String.format;

/**
 * Enum représentant les codes d'erreur fonctionnels
 */
@Getter
public enum FunctionalErrorCode {
  NOT_NULL_FIELDS(1, "Les champs suivants sont obligatoires : %s"),
  INCORRECT_LENGTH_FIELDS(2, "Les champs suivants n'ont pas la bonne taille : %s"),
  NO_BLANK_FIELDS(3, "Les champs suivants ne doivent pas être vide : %s"),
  FORBIDDEN_ACTION(4, "Action non autorisée"),
  DIACTIVATED_ENTITY(5, "L'entité %s avec l'id %s est déactivée"),
  UNKNOWN_ENTITY(6, "L'entité %ss avec l'id %s est inconnu"),
  INCORRECT_VERSION(7, "Modification non applicable, version non conforme"),
  SIZE_LIMIT_EXCEEDED(8, "La taille d'un document ne peut être supérieure à %s"),
  MALFORMED_REQUEST(9, "Requête mal formatée"),
  PATTERN_FIELDS(10, "Les champs suivants ne respectent pas le bon format : %s"),
  INVALID_EMAIL(11, "Le champs suivant ne respecte pas le format d'email : %s"),
  MULTIPLE_USERS_FOUND_WITH_EMAIL(12, "%s"),
  REQUIRED_PASSWORD(13, "Activation impossible, pas de mot de passe défini"),
  INVALID_PASSWORD(14, "Le mot de passe %s est invalide"),
  UNKNOWN_USER(15, "L'utilisateur avec l'email %s est inconnu"),
  UNKNOWN_TOKEN(16, "Le token avec la valeur %s est inconnu"),
  NOT_FOUND_ENTITY(17, "Aucun enregistrement de type %s et avec l'id %s n'est présent dans la base de données"),
  UNKNOWN_USERNAME(18, "L'utilisateur avec le nom d'utilisateur %s est inconnu"),
  CANNOT_CHANGE_STATUS(19, "Le changement de statut ne peut être effectué de : %s à : %s"),
  INVALID_TOKEN(20, "Le token %s est invalide"),
  NOT_ACTIVE_ACCOUNT(21, "Le compte utilisateur d'id %s n'est pas actif"),
  MULTIPLE_USERS_FOUND_WITH_USERNAME(22, "Plusieurs utilisateurs trouvés avec le nom d'utilisateur %s"),
  MULTIPLE_USERS_FOUND_WITH_ID(23, "Plusieurs utilisateurs trouvés avec l'id %s"),

  UNIQUENESS_CONSTRAINT_NOT_RESPECTED(26, "Contrainte d'unicité non respectée sur les champs suivants : %s"),
  UNKNOWN_USER_BY_ID(27, "L'utilisateur avec l'identifiant %s est inconnu");


  private final String code;
  private final String messageTemplate;

  FunctionalErrorCode(int code, String messageTemplate) {
    this.code = format("%03d", code);
    this.messageTemplate = messageTemplate;
  }
}
