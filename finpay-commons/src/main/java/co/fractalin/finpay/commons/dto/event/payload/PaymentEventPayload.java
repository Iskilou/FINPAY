package co.fractalin.finpay.commons.dto.event.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentEventPayload {
  private String id;
  private String accountStatus;
  private BigDecimal amount;
  private String accountId;
  private Instant operationDate;
}
