package co.fractalin.finpay.commons.dto.event.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserEventPayload {
  private String id;
  private String username;
  private String organizationId;
  private String accountId;
  private String userId;
  private String status;
}
