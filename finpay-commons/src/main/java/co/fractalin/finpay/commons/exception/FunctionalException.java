package co.fractalin.finpay.commons.exception;

import co.fractalin.finpay.commons.dto.error.ErrorMessage;
import co.fractalin.finpay.commons.dto.error.RestApiError;
import co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import static java.lang.String.format;

/**
 * Cette classe représente une exception de type fonctionnel.
 */
@EqualsAndHashCode(callSuper = true)
public class FunctionalException extends RuntimeException {
  @Getter
  private final FunctionalErrorCode errorCode;

  FunctionalException(String messageTemplate, FunctionalErrorCode errorCode, Throwable cause, String... arguments) {
    super(format(messageTemplate, arguments), cause);
    this.errorCode = errorCode;
  }

  FunctionalException(FunctionalErrorCode errorCode, String... arguments) {
    this(errorCode.getMessageTemplate(), errorCode, null, arguments);
  }

  public FunctionalException(FunctionalErrorCode errorCode, Throwable cause, String... arguments) {
    this(errorCode.getMessageTemplate(), errorCode, cause, arguments);
  }

  public ErrorMessage toErrorMessage() {
    return new ErrorMessage(new RestApiError(errorCode.getCode(), getMessage()));
  }
}
