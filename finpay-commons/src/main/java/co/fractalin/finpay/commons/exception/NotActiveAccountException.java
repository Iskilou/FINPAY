package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.NOT_ACTIVE_ACCOUNT;
import static java.lang.String.valueOf;

public class NotActiveAccountException extends FunctionalException {
  public NotActiveAccountException(String userId) {
    super(NOT_ACTIVE_ACCOUNT, valueOf(userId));
  }

}
