package co.fractalin.finpay.commons.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static lombok.AccessLevel.PROTECTED;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = PROTECTED)
public abstract class PersonDto {

  @NotNull
  private String firstName;
  @NotNull
  private String lastName;
  @NotNull
  @JsonSerialize(using = LocalDateSerializer.class)
  private LocalDate birthDate;
  @NotNull
  private String gender;
  private String phoneNumber;
  private String zipCode;
  private String street;
  private String cityId;
  private String userId;
}
