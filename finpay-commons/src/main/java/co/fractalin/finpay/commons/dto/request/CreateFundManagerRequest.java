package co.fractalin.finpay.commons.dto.request;

import co.fractalin.finpay.commons.dto.FundManagerDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.FUNDMANAGER_DETAIL;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateFundManagerRequest {

  @NotNull
  @ApiModelProperty(FUNDMANAGER_DETAIL)
  private FundManagerDto fundManagerDTO;
}
