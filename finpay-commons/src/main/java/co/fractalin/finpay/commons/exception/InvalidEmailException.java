package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.INVALID_EMAIL;

public class InvalidEmailException extends FunctionalException {

  public InvalidEmailException(String field) {
    super(INVALID_EMAIL, field);
  }
}
