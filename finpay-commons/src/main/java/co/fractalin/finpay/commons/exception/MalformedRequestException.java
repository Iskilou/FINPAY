package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.MALFORMED_REQUEST;

public class MalformedRequestException extends FunctionalException {
  public MalformedRequestException() {
    super(MALFORMED_REQUEST);
  }
}
