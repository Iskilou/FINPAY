package co.fractalin.finpay.commons.utils;


import java.util.UUID;

/**
 * Classe utilitaire pour la base de données
 */
public class DatabaseUtils {

  public static String generateId() {
    return UUID.randomUUID().toString();
  }

}
