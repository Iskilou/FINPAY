package co.fractalin.finpay.commons.dto.event;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class FinpayDomainEventType {
  public static final String PAYMENT_EVENT = "payment-event";
  public static final String EMAIL_SENDING_EVENT= "email-sending-event";
  public static final String USER_CREATION_EVENT= "user-creation-event";

}
