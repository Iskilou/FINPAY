package co.fractalin.finpay.commons.common.view;

/**
 * Vue par défaut utilisée pour les {@code @JsonView}.
 * Les vues spécifiques doivent étendre cette interface pour être prise en compte dans la sérialisation des {@code Page}.
 */
public interface DefaultView {
}
