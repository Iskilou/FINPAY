package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.UNIQUENESS_CONSTRAINT_NOT_RESPECTED;

/**
 * Exception déclenchée lors d'une validation d'entité : cette entité contient au minimum un champ ne respectant pas une des contraintes d'unicité
 */
public class UniquenessConstraintException extends FunctionalException {

    public UniquenessConstraintException(String fields) {
        super(UNIQUENESS_CONSTRAINT_NOT_RESPECTED, fields);
    }
}
