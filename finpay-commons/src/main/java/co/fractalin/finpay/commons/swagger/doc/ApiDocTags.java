package co.fractalin.finpay.commons.swagger.doc;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PROTECTED;

/**
 * Classe utilitaire contenant les tags des opérations utilisées pour la
 * documentation de l'API
 */
@NoArgsConstructor(access = PROTECTED)
public final class ApiDocTags {

  private static final String WEBSERVICES = "Webservices ";
  private static final String WEBSERVICES_FOR = WEBSERVICES + "des ";
  public static final String AUTHENTICATION = WEBSERVICES + "d authentification";
  public static final String FUNDMANAGERS = WEBSERVICES_FOR + "institutions financières";
  public static final String ORGANISATIONS = WEBSERVICES + "de gestions des organisations ";
}
