package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.NOT_NULL_FIELDS;

public class NullValueException extends FunctionalException {

  public NullValueException(String fields) {
    super(NOT_NULL_FIELDS, fields);
  }
}
