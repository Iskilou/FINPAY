package co.fractalin.finpay.commons.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FundManagerDto {
  private String id;
  private String name;
  private String accountId;
  private BigDecimal amount;
  private String currency;
}
