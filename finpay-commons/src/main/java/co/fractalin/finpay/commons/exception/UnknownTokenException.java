package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.UNKNOWN_TOKEN;

public class UnknownTokenException extends FunctionalException {
  public UnknownTokenException(String value) {
    super(UNKNOWN_TOKEN, value);
  }
}
