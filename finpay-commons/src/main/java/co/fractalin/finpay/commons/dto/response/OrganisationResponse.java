package co.fractalin.finpay.commons.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import co.fractalin.finpay.commons.dto.OrganizationDto;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.ORGANISATION_DETAIL;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrganisationResponse implements Serializable {
  @Valid
  @ApiModelProperty(ORGANISATION_DETAIL)
  private OrganizationDto organizationDto;

}
