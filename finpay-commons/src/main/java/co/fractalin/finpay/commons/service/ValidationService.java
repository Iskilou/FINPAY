package co.fractalin.finpay.commons.service;

import com.google.common.collect.ImmutableMap;
import co.fractalin.finpay.commons.exception.FunctionalException;
import co.fractalin.finpay.commons.exception.IncorrectLengthException;
import co.fractalin.finpay.commons.exception.InvalidEmailException;
import co.fractalin.finpay.commons.exception.NotBlankException;
import co.fractalin.finpay.commons.exception.NullValueException;
import co.fractalin.finpay.commons.exception.PatternMismatchException;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.concat;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;

/**
 * Service qui permet de valider les données d'une entité à l'aide de Hibernate Validator
 */
@Service
@RequiredArgsConstructor
public class ValidationService {
  public static final ImmutableMap<Class<?>, Function<String, ? extends FunctionalException>> EXCEPTION_PROVIDERS =
      ImmutableMap.<Class<?>, Function<String, ? extends FunctionalException>>builder()
        .put(NotNull.class, NullValueException::new)
        .put(Length.class, IncorrectLengthException::new)
        .put(NotBlank.class, NotBlankException::new)
        .put(Email.class, InvalidEmailException::new)
        .put(Pattern.class, PatternMismatchException::new)
      .build();

  private static final Collection<Class<?>> CONSTRAINTS_PRIORITIES = EXCEPTION_PROVIDERS.keySet();

  private final Validator validator;

  public <T> void validate(T entity, Class<?>... groups) throws FunctionalException {
      Class<?>[] groupsWithDefault = concat(Stream.of(groups), Stream.of(Default.class)).toArray(Class<?>[]::new);

      Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity, groupsWithDefault);

      Map<Class<?>, List<ConstraintViolation<T>>> violationsByConstraint = constraintViolations.stream().collect(toMap(
              ValidationService::getConstraintAnnotation,
              ValidationService::newLinkedList,
              ValidationService::mergeLists));

      if (isNotEmpty(violationsByConstraint)) {
          throw CONSTRAINTS_PRIORITIES.stream()
                  .filter(violationsByConstraint::containsKey)
                  .findFirst()
                  .map(constraint -> {
                      Function<String, ? extends FunctionalException> provider = EXCEPTION_PROVIDERS.get(constraint);
                      String propertyNames = violationPropertyNames(violationsByConstraint.get(constraint));
                      return provider.apply(propertyNames);
                  })
                  .map(RuntimeException.class::cast)
                  .orElse(new ConstraintViolationException("Violations de contraintes sans code d'erreur", constraintViolations));
      }
  }

  private static <T> Class<? extends Annotation> getConstraintAnnotation(ConstraintViolation<T> violation) {
    return violation.getConstraintDescriptor().getAnnotation().annotationType();
  }

  private static <E>LinkedList<E> newLinkedList(E element) {
    LinkedList<E> list = new LinkedList<>();
    list.add(element);
    return list;
  }

    private static <E> List<E> mergeLists(List<E> a, List<E> b) {
        a.addAll(b);
        return a;
    }

    private static <T> String violationPropertyNames(Collection<ConstraintViolation<T>> violations) {
        return violations.stream()
                .map(ConstraintViolation::getPropertyPath)
                .map(Path::toString)
                .sorted()
                .collect(joining(", "));
    }
}
