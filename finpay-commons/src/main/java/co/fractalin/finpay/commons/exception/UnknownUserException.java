package co.fractalin.finpay.commons.exception;

import java.util.UUID;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.UNKNOWN_USER;
import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.UNKNOWN_USER_BY_ID;

/**
 * Exception déclenchée lorsqu'un utilisateur est inconnu du système
 */
public class UnknownUserException extends FunctionalException {
  public UnknownUserException(String email) {
    super(UNKNOWN_USER, email);
  }
  public UnknownUserException(UUID uuid) {
    super(UNKNOWN_USER_BY_ID, uuid.toString());
  }
}
