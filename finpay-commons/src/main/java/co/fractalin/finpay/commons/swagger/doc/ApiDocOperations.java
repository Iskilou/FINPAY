package co.fractalin.finpay.commons.swagger.doc;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PROTECTED;

/**
 * Classe utilitaire contenant les descriptions des opérations des contrôleurs
 * utilisées pour la documentation de l'API
 */
@NoArgsConstructor(access = PROTECTED)
public final class ApiDocOperations {

  public static final String AUTHENTICATE = "Authentification";

  public static final String REFRESH_AUTHENTICATION = "Rafraîchissement du token d'authentification";
  public static final String USER_DETAIL = "Detail sur les informations de connexion";
  public static final String USER_ID = "Identifiant de l'utilisateur";
  public static final String USER_MAIL = "Email de l'utilisateur";
  public static final String ACCOUNT_DETAIL = "Detail sur le compte";
  public static final String PARAMETER_DETAIL = "Detail sur les paramètres de l'organisation";
  public static final String COUNT_ENTITY = "Nombre d'occurence de l'entité trouvé";
  /**
   * ************************** ORGANISATION *************************
   */
  public static final String FIND_ORGANISATIONS = "Liste des organisations ";
  public static final String ORGANISATION_DETAIL = "Detail sur l'organisation";
  public static final String ORGANIZATION_ID = "Identifiant du l'organisation";


  /**
   * ************************** FUND_MANAGER ****************************
   */
  public static final String CREATE_FUNDMANAGER = "Création d'une institution financière";
  public static final String UPDATE_FUNDMANAGER = "Mise à jour d'une institution financière";
  public static final String FIND_FUNDMANAGERS = "Liste des institutions financières";
  public static final String FIND_FUNDMANAGER = "Lire une institution financière";
  public static final String FUNDMANAGER_DETAIL = "Detail sur l'institution financière";
  public static final String FUNDMANAGER_ID = "Identifiant de l'institution financière";
  public static final String CREATE_FUNDMANAGER_REQUEST = "Requête de création d'une institution financière";
  public static final String UPDATE_FUNDMANAGER_REQUEST = "Requête de mise à jour d'une institution financière";


}
