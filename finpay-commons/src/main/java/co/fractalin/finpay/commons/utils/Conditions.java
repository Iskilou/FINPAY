package co.fractalin.finpay.commons.utils;

import lombok.NoArgsConstructor;
import org.assertj.core.api.Condition;

import java.time.OffsetDateTime;
import java.time.temporal.Temporal;

import static java.lang.Math.abs;
import static java.time.temporal.ChronoUnit.SECONDS;
import static lombok.AccessLevel.PRIVATE;

/**
 * Regroupe les conditions personnalisées fréquemment utilisées
 */
@NoArgsConstructor(access = PRIVATE)
public final class Conditions {
    static final int DEFAULT_DELTA = 20;

    /**
     * Crée une condition applicable à une date java 8, qui est vraie si l'écart entre la date testée et {@code other} est inférieur à {@code delta}.
     * Exemple:
     * <pre>{@code
     *     assertThat(dateTime).is(closeTo(now(), 1));
     * }</pre>
     * @param delta l'écart en secondes
     */
    public static <T extends Temporal> Condition<T> closeTo(T other, long delta) {
        return new Condition<>(
                t -> t != null && abs(t.until(other, SECONDS)) < delta,
                "close to %s (delta: %ds)", other, delta);
    }

    /**
     * Crée une condition applicable à une date java 8, qui est vraie si l'écart entre la date testée et {@code other} est inférieur à
     * {@code DEFAULT_DELTA} secondes. Exemple:
     * <pre>{@code
     *     assertThat(dateTime).is(closeTo(now()));
     * }</pre>
     */
    public static <T extends Temporal> Condition<T> closeTo(T other) {
        return closeTo(other, DEFAULT_DELTA);
    }

    /**
     * Crée une condition applicable à une date java 8, qui est vraie si la date testée est proche (à {@code DEFAULT_DELTA} secondes près)
     * de l'instant où la méthode est appelée. Exemple:
     * <pre>{@code
     *     assertThat(dateTime).is(closeToNow());
     * }</pre>
     */
    public static <T extends Temporal> Condition<T> closeToNow() {
        return closeTo((T) OffsetDateTime.now());
    }
}
