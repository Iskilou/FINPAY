package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.FORBIDDEN_ACTION;

/**
 * Exception déclenchée lors d'une tentative non autorisée par l'utilisateur
 */
public class ForbiddenActionException extends FunctionalException {

  public ForbiddenActionException() {
    super(FORBIDDEN_ACTION);
  }
}
