package co.fractalin.finpay.commons.dto.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Cette classe va sérializer une erreur HTTP avec son code et son message.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestApiError implements Serializable {
  private String code;
  private String message;
}
