package co.fractalin.finpay.commons.dto.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe premettant de sérialiser un message d'erreur.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
  private RestApiError error;
}
