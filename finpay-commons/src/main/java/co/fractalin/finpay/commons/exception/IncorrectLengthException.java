package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.INCORRECT_LENGTH_FIELDS;

/**
 * Exception déclenchée lors d'une validation d'entité : au moins un des champs de l'entité n'a pas une taille valide
 */
public class IncorrectLengthException extends FunctionalException {

  public IncorrectLengthException(String fields) {
    super(INCORRECT_LENGTH_FIELDS, fields);
  }
}
