package co.fractalin.finpay.commons.dto.command;

/**
 * Liste des noms de commandes
 */
public class Commands {


  public static final String CREATE_FUNDMANAGER = "createFundManager";
  public static final String UPDATE_FUNDMANAGER = "updateFundManager";

}
