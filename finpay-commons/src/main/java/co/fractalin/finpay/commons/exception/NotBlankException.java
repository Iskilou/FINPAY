package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.NO_BLANK_FIELDS;

public class NotBlankException extends FunctionalException {
  public NotBlankException(String fields) {
    super(NO_BLANK_FIELDS, fields);
  }
}
