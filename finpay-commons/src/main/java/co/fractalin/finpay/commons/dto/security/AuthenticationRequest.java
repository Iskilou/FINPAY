package co.fractalin.finpay.commons.dto.security;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.PASSWORD;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.USERNAME;
import static lombok.AccessLevel.PROTECTED;

/**
 * DTO utilisé par la requête d'authentification sur l'API
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(access = PROTECTED)
public abstract class AuthenticationRequest implements Serializable {

  @ApiModelProperty(USERNAME)
  private String username;

  @ApiModelProperty(PASSWORD)
  private String password;
}
