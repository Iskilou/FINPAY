package co.fractalin.finpay.commons.common;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

/**
 * Classe utilitaire définissant les constantes utilisées par tous les modules.
 */
@NoArgsConstructor(access = PRIVATE)
public class CommonConstant {
  public static final String SPRING_PROFILE_DEFAULT = "default";
  public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
  public static final String SPRING_PROFILE_TEST = "test";
  public static final String SPRING_PROFILE_PRODUCTION = "prod";

  /** valeur à donner à @Column.columnDefinition des champs booléens pour que la valeur par défaut de la colonne soit false */
  public static final String DEFAULT_FALSE = "bit default 0";

  /** valeur à donner à @Column.columnDefinition des champs Integer pour que la valeur par défaut de la colonne soit 0 */
  public static final String DEFAULT_ZERO = "Integer default 0";

  public static final String VARCHAR = "varchar(";

  public static final String JSON_HOUR_MINUTES_PATTERN = "HH:mm";

}
