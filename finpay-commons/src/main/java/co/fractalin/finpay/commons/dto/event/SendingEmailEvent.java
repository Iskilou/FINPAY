package co.fractalin.finpay.commons.dto.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

import static co.fractalin.finpay.commons.dto.event.FinpayDomainEventType.EMAIL_SENDING_EVENT;

/**
 * Classe de gestion des événements de type  mail
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendingEmailEvent implements Serializable {

  private String type;
  private Map<String, Object> context;

  @Builder
  public SendingEmailEvent(Map<String, Object> context) {
    this.type = EMAIL_SENDING_EVENT;
    this.context = context;

  }
  }
