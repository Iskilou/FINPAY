package co.fractalin.finpay.commons.dto.command.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import co.fractalin.finpay.commons.dto.request.UpdateFundManagerRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * commande  de mise à jour d'une institution  financière
 */
@Getter
@Setter
@ToString
public class UpdateFundManagerCommandRequest implements CommandRequest {

  private UpdateFundManagerRequest updateFundManagerRequest;

  public UpdateFundManagerCommandRequest() {
  }

  @Builder
  public UpdateFundManagerCommandRequest(UpdateFundManagerRequest updateFundManagerRequest) {
    this.updateFundManagerRequest = updateFundManagerRequest;
  }

  @Override
  public UpdateFundManagerCommandRequest apply(String object, ObjectMapper mapper) {
    try {
      return mapper.readValue(object, UpdateFundManagerCommandRequest.class);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
