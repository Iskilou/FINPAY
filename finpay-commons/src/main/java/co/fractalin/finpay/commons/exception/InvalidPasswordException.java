package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.INVALID_PASSWORD;

public class InvalidPasswordException extends FunctionalException {

  public static final String OLD_PASSWORD_LABEL = "actuel";

  public InvalidPasswordException(String name) {
    super(INVALID_PASSWORD, name);
  }
}
