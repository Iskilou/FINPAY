package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.SIZE_LIMIT_EXCEEDED;

public class SizeLimitExceededException extends FunctionalException {
  public SizeLimitExceededException(String maxFileSize) {
    super(SIZE_LIMIT_EXCEEDED, maxFileSize);
  }
}
