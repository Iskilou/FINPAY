package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.CANNOT_CHANGE_STATUS;

public class IllegalStatusChangeException extends FunctionalException {
  public <T extends Enum<?>> IllegalStatusChangeException(T currentStatus, T wantedStatus) {
    super(CANNOT_CHANGE_STATUS, currentStatus.name(), wantedStatus.name());
  }
}
