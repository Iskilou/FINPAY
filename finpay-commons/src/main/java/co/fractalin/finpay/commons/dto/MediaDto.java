package co.fractalin.finpay.commons.dto;

import java.nio.ByteBuffer;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * The type Mail.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MediaDto {

    private String id;
    private String originalName;
    private String internalName;
    private String type;
    private ByteBuffer content;

}
