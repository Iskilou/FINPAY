package co.fractalin.finpay.commons.dto.response;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.COUNT_ENTITY;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CountResponse {

  @NotNull
  @ApiModelProperty(COUNT_ENTITY)
  private long nbr;

}
