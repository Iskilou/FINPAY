package co.fractalin.finpay.commons.service;

import co.fractalin.finpay.commons.exception.NotFoundException;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

import static org.springframework.core.GenericTypeResolver.resolveTypeArguments;

public class CrudService<T, R extends CassandraRepository<T, String>> {

  private final Class<T> entityClass;
  protected final R repository;

  @SuppressWarnings("unchecked")
  public CrudService(R repository) {
    this.repository = repository;
    entityClass = (Class<T>) Objects.requireNonNull(resolveTypeArguments(getClass(), CrudService.class))[0];
  }

  @Transactional
  @LogArgumentsAndResult
  public T update(String id, T entity) {
    findById(id);
    return repository.save(entity);
  }

  public T findById(String id) {
    return repository.findById(id)
        .orElseThrow(() -> new NotFoundException(entityClass, id));
  }
}
