package co.fractalin.finpay.commons.exception;

import static co.fractalin.finpay.commons.exception.errorcode.FunctionalErrorCode.REQUIRED_PASSWORD;

public class RequiredPasswordException extends FunctionalException {
  public RequiredPasswordException() {
    super(REQUIRED_PASSWORD);
  }
}
