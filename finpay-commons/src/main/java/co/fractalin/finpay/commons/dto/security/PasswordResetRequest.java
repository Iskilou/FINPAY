package co.fractalin.finpay.commons.dto.security;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.PASSWORD;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.TOKEN;

/**
 * DTO utilisé par la requête de réinitialisation du mot de passe d'un compte utilisateur lorsque le mot de passe oublié
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasswordResetRequest {
  @ApiModelProperty(PASSWORD)
  @NotBlank
  private String password;

  @ApiModelProperty(TOKEN)
  @NotBlank
  private String token;
}
