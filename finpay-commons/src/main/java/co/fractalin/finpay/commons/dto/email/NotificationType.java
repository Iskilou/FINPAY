package co.fractalin.finpay.commons.dto.email;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class NotificationType {

  public static final String EVENT_CREATION_ALERT = "created-event-alert";
  public static final String EVENT_FUND_WITHDRAWL_ALERT = "fund-withdrawl-event-alert";

}
