package co.fractalin.finpay.commons.mapper;

import co.fractalin.finpay.commons.dto.FundManagerDto;
import co.fractalin.finpay.model.entity.FundManager;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FundManagerToFundManagerDtoMapper {
  FundManagerDto fromFundManagerToFundManagerDTO(FundManager fundManager);
  FundManager fromFundManagerDTOToFundManager(FundManagerDto fundManagerDTO);
}
