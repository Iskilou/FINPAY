package co.fractalin.finpay.commons.utils;

import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.OffsetDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.MapperFeature.USE_GETTERS_AS_SETTERS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;

/**
 * Classe utilitaire pour la gestion d'un JavaTimeModule pour sérializer des dates avec le décalage horaire en UTC.
 */
@NoArgsConstructor(access = PRIVATE)
public final class JacksonUtils {

  public static ObjectMapper mapper() {
    ObjectMapper mapper = new ObjectMapper();

    mapper.registerModule(getJavaTimeModule());
    mapper.registerModule(new JavaTimeModule());
    mapper.registerModule(new ParameterNamesModule());
    mapper.disable(USE_GETTERS_AS_SETTERS);
    mapper.disable(WRITE_DATES_AS_TIMESTAMPS);
    mapper.setSerializationInclusion(NON_NULL);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    return mapper;
  }

  public static JavaTimeModule getJavaTimeModule() {
    JavaTimeModule module = new JavaTimeModule();
    module.addSerializer(ZonedDateTime.class, new UTCZonedDateTimeSerializer());
    module.addSerializer(OffsetDateTime.class, new UTCOffsetDateTimeSerializer());
    return module;
  }

  @NoArgsConstructor
  private static final class UTCZonedDateTimeSerializer extends ZonedDateTimeSerializer {
    private static final long serialVersionUID = 6375024921930850900L;

    private UTCZonedDateTimeSerializer(UTCZonedDateTimeSerializer base, Boolean useTimestamp, DateTimeFormatter formatter, Boolean writeZoneId) {
      super(base, useTimestamp, formatter, writeZoneId);
    }

    @Override
    public void serialize(ZonedDateTime value, JsonGenerator generator, SerializerProvider provider) throws IOException {
      super.serialize(value.withZoneSameInstant(UTC), generator, provider);
    }

    @Override
    protected UTCZonedDateTimeSerializer withFormat(Boolean useTimestamp, DateTimeFormatter formatter, Shape shape) {
      return new UTCZonedDateTimeSerializer(this, useTimestamp, formatter, _writeZoneId);
    }
  }

  @NoArgsConstructor
  private static final class UTCOffsetDateTimeSerializer extends OffsetDateTimeSerializer {
    private static final long serialVersionUID = -7451301106426507725L;

    private UTCOffsetDateTimeSerializer(OffsetDateTimeSerializer base, Boolean useTimestamp, DateTimeFormatter formatter) {
      super(base, useTimestamp, formatter);
    }

    @Override
    public void serialize(OffsetDateTime value, JsonGenerator generator, SerializerProvider provider) throws IOException {
      super.serialize(value.withOffsetSameInstant(UTC), generator, provider);
    }

    @Override
    protected UTCOffsetDateTimeSerializer withFormat(Boolean useTimestamp, DateTimeFormatter formatter, Shape shape) {
      return new UTCOffsetDateTimeSerializer(this, useTimestamp, formatter);
    }
  }
}
