package co.fractalin.finpay.commons.utils;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.stubbing.Answer;
import org.testng.annotations.DataProvider;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static lombok.AccessLevel.PRIVATE;
import static org.assertj.core.api.Assertions.fail;

/**
 * Regroupe les méthodes de test fréquemment utilisées
 */
@NoArgsConstructor(access = PRIVATE)
public class CommonTestUtils {
  public static final String BOOLEAN_PROVIDER = "booleanProvider";
  public static final String TWO_BOOLEANS_PROVIDER = "twoBooleansProvider";

  public static void failIfNoException() throws AssertionError {
    fail("Une exception aurait dû être levée");
  }

  public static Answer<?> returnArgument() {
    return returnArgument(0);
  }

  public static Answer<?> returnArgument(int index) {
    return invocation -> invocation.getArgument(index, invocation.getMethod().getReturnType());
  }

  public static Object[][] combine(Map<?, ? extends Collection<?>> values) {
    return values.entrySet().stream()
        .flatMap(entry -> combine(entry.getKey(), entry.getValue()))
        .toArray(Object[][]::new);
  }

  public static Stream<Object[]> combine(Object from, Collection<?> tos) {
    return tos.stream().map(to -> new Object[] {from, to});
  }

  /**
   * Transforme chaque valeur d'une map en utilisant un valueMapper qui peut donner des valeurs nulles.
   * /!\ Cette méthode n'est pas optimisée et doit être utilisée uniquement dans le cadre des tests.
   * @param map la map contenant les valeurs à transformer
   * @param valueMapper le valueMapper utilisé pour transformer les valeurs
   * @return une map contenant des valeurs transformées en utilisant un valueMapper
   */
  public static <T> Map<String, T> transformValues(Map<String, String> map, Function<String, T> valueMapper) {
    return map.entrySet().stream()
        .map(entry -> ImmutablePair.of(entry.getKey(), valueMapper.apply(entry.getValue())))
        .collect(LinkedHashMap::new, (resultMap, pair) -> resultMap.put(pair.getLeft(), pair.getRight()), LinkedHashMap::putAll);
  }

  @DataProvider(name = BOOLEAN_PROVIDER)
  public static Object[][] booleanProvider() {
    return new Object[][] {{true}, {false}};
  }

  @DataProvider(name = TWO_BOOLEANS_PROVIDER)
  public static Object[][] twoBooleansProvider() {
    return new Object[][] {{true, true}, {true, false}, {false, true}, {false, false}};
  }
}
