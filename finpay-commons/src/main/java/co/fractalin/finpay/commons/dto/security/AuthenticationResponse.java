package co.fractalin.finpay.commons.dto.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.AUTH_TOKEN;

/**
 * Réponse à la requête d'authentification sur lAPI
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse implements Serializable {

  @ApiModelProperty(AUTH_TOKEN)
  @JsonProperty("id_token")
  private String token;
}
