package co.fractalin.finpay.commons.dto.command.request;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

public interface CommandRequest extends Serializable {
  CommandRequest apply(String object, ObjectMapper mapper);
}
