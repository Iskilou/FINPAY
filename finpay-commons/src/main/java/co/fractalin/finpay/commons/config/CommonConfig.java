package co.fractalin.finpay.commons.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import co.fractalin.finpay.model.enumeration.Role;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static co.fractalin.finpay.commons.utils.JacksonUtils.mapper;
import static java.util.regex.Pattern.compile;
import static java.util.regex.Pattern.quote;


@Configuration
@Import(FinpayMapStructConfig.class)
public class CommonConfig {

  private static final Pattern LIST_SPLITTER = compile(quote("|"));

  @Bean
  public Validator validator() {
    return new LocalValidatorFactoryBean();
  }

  @Bean
  public ConversionService conversionService() {
    DefaultConversionService service = new DefaultConversionService();

    service.addConverter(String.class, Integer[].class, source -> Stream.of(LIST_SPLITTER.split(source))
        .map(Integer::parseInt)
        .toArray(Integer[]::new));

    service.addConverter(String.class, Role[].class, source -> Stream.of(LIST_SPLITTER.split(source))
        .map(Role::valueOf)
        .toArray(Role[]::new));

    return service;
  }

  @Bean
  @Primary
  public ObjectMapper objectMapper() {
    return mapper();
  }





}
