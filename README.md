[**Documentation sur l'architecture**](https://gitlab.com/cyrilowona/pcnv2/-/wikis/Documentation-du-Backend)

[**Démarrage du backend**](https://gitlab.com/cyrilowona/pcnv2/-/wikis/D%C3%A9marrage-du-Backend)


Service de Notification:
Configuration des paramètres d'envoi de mail dans le fichier 
application.yml du module pcnv2-notifications