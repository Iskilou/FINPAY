package co.fractalin.finpay.core.config.command;

import co.fractalin.finpay.core.handler.CommandHandler;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.util.ClassUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CommandHandlerUtils {
  public static Map<String, CommandHandler> buildCommandHandlersRegistry(final String basePackage,
                                                                         final ApplicationContext context) {

    final Map<String, CommandHandler> registry = new HashMap<>();
    final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
    final AutowireCapableBeanFactory beanFactory = context.getAutowireCapableBeanFactory();
    scanner.addIncludeFilter(new AssignableTypeFilter(CommandHandler.class));

    for (BeanDefinition bean : scanner.findCandidateComponents(basePackage)) {
      CommandHandler currentHandler = (CommandHandler) beanFactory.createBean(
          ClassUtils.resolveClassName(Objects.requireNonNull(bean.getBeanClassName()), context.getClassLoader()),
          AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);

      registry.put(currentHandler.getName(), currentHandler);
    }

    return registry;
  }
}
