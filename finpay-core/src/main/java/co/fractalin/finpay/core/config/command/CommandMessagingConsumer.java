package co.fractalin.finpay.core.config.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import co.fractalin.finpay.core.handler.CommandHandler;
import co.fractalin.finpay.core.util.CommandRequestModelFactory;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;

/**
 * Utilitaire permettant de lancer les consumers kafka en mode asynchrone
 */
public class CommandMessagingConsumer implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(CommandMessagingConsumer.class);
  private final Duration timeout;
  private final Map<String, CommandHandler> handlerRegistry;
  private final String topic;
  private final KafkaProperties properties;
  private final ObjectMapper mapper;

  public CommandMessagingConsumer(Duration timeout,
                                  Map<String, CommandHandler> handlerRegistry,
                                  String topic,
                                  KafkaProperties properties, ObjectMapper mapper) {
    this.timeout = timeout;
    this.handlerRegistry = handlerRegistry;
    this.topic = topic;
    this.properties = properties;
    this.mapper = mapper;
  }

  @Override
  public void run() {
    ConsumerRecords<String, String> events;

    long pollTimeout = this.timeout.getSeconds();
    KafkaConsumer<String, String> consumer = getKafkaCommandConsumer();

    log.info("Recherche des gestionnaires d'événements : {}", printRegistry(this.handlerRegistry));

    while (true) {

      events = consumer.poll(this.timeout);
      if (events != null && events.count() == 0) {
        pollTimeout++;
      } else {
        assert events != null;
        log.info("Appelé pour la {} fois afin de gérer un événement après {}(s)", events.count(), pollTimeout);
        pollTimeout = this.timeout.getSeconds();

        events.forEach(event -> {
          CommandHandler handler = this.handlerRegistry.get(event.key());
          if (handler != null) {
              handler.execute(CommandRequestModelFactory.createInstance(event.value(), handler.getName(), mapper));
          } else {
            log.warn("Impossible de trouver un gestionnaire pour l'événement : {}", event.key());
          }
        });
      }
    }
  }

  private KafkaConsumer<String, String> getKafkaCommandConsumer() {
    KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties.getConsumerProps());
    consumer.subscribe(Collections.singletonList(this.topic));

    return consumer;
  }

  private static String printRegistry(final Map<String, CommandHandler> registry) {
    final String separator = System.getProperty("line.separator");
    final StringBuilder builder = new StringBuilder();

    registry.forEach((key, value) -> {
      builder.append(String.format("%s -> %s", key, value.getClass().getName()));
      builder.append(separator);
    });

    return builder.toString();
  }
}
