package co.fractalin.finpay.core.initializer;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Service d'initialisation des paramètres par default
 */
@Component
@Profile("initializer")
@RequiredArgsConstructor
public class DefaultParametersInitializer {

  private static final BigDecimal COMMUNITY_MIN_AMOUNT = BigDecimal.valueOf(1000);
  private static final BigDecimal MEMBER_MIN_AMOUNT = BigDecimal.valueOf(200);



}
