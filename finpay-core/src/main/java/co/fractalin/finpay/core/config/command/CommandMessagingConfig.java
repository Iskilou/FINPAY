package co.fractalin.finpay.core.config.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import co.fractalin.finpay.core.handler.CommandHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;

/**
 * Classe de configuration pour enregistrer les queues pour les bus de commandes
 */
@Configuration
public class CommandMessagingConfig {

  private final ApplicationContext context;
  private final TaskExecutor consumerTaskExecutor;
  private final String topic;
  private final String commandHandlerPackageBase;
  private final Duration timeout;
  private final KafkaProperties properties;
  private final ObjectMapper mapper;

  public CommandMessagingConfig(ApplicationContext context,
                                TaskExecutor consumerTaskExecutor,
                                @Value("${application.kafka.command.topic}") String topic,
                                @Value("${application.command.handler.package.base}") String commandHandlerPackageBase,
                                @Value("${application.kafka.consumer.timeout}") Long timeout,
                                KafkaProperties properties, ObjectMapper mapper) {
    this.context = context;
    this.consumerTaskExecutor = consumerTaskExecutor;
    this.topic = topic;
    this.commandHandlerPackageBase = commandHandlerPackageBase;
    this.timeout = Duration.ofSeconds(timeout);
    this.properties = properties;
    this.mapper = mapper;
  }

  @PostConstruct
  private void setup() {
    final Map<String, CommandHandler> registry = CommandHandlerUtils.buildCommandHandlersRegistry(this.commandHandlerPackageBase, this.context);

    CommandMessagingConsumer eventConsumer = new CommandMessagingConsumer(timeout, registry, topic, properties, mapper);

    this.consumerTaskExecutor.execute(eventConsumer);
  }
}
