package co.fractalin.finpay.core.config;

import co.fractalin.finpay.model.FinpayModelConfig;
import com.google.gson.Gson;
import co.fractalin.finpay.commons.config.CommonConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
@ComponentScan
@Import(value = {FinpayModelConfig.class, CommonConfig.class})
public class CoreConfig {
  @Bean
  public Gson gson() {
    return new Gson();
  }

  @Bean
  public TaskExecutor consumerTaskExecutor() {
    return new SimpleAsyncTaskExecutor();
  }
}
