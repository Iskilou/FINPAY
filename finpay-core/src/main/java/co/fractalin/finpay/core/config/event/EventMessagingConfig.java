package co.fractalin.finpay.core.config.event;

import co.fractalin.finpay.core.config.command.KafkaProperties;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class EventMessagingConfig {

  private final KafkaProperties properties;

  @Bean
  public KafkaProducer<String, String> getProducerProps() {
    return new KafkaProducer<>(properties.getProducerProps());
  }
}
