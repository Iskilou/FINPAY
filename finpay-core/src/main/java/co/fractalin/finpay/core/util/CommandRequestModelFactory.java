package co.fractalin.finpay.core.util;

import co.fractalin.finpay.commons.dto.command.request.CommandRequest;
import co.fractalin.finpay.commons.dto.command.request.CreateFundManagerCommandRequest;
import co.fractalin.finpay.commons.dto.command.request.UpdateFundManagerCommandRequest;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static co.fractalin.finpay.commons.dto.command.Commands.CREATE_FUNDMANAGER;
import static co.fractalin.finpay.commons.dto.command.Commands.UPDATE_FUNDMANAGER;


/**
 * Convertisseur de {@link Map} à {@link <? extends CommandRequest>}
 */
public class CommandRequestModelFactory {

  static Map<String, CommandRequest> commandRequestMap = new HashMap<>();

  static {
    // TODO: Ajouter toutes les requêtes et le nom des commandes correspondantes

    commandRequestMap.put(CREATE_FUNDMANAGER, new CreateFundManagerCommandRequest());
    commandRequestMap.put(UPDATE_FUNDMANAGER, new UpdateFundManagerCommandRequest());

  }

  public static Optional<CommandRequest> getRequest(String commandName) {
    return Optional.ofNullable(commandRequestMap.get(commandName));
  }

  @LogArgumentsAndResult
  public static CommandRequest createInstance(String object, String commandName, ObjectMapper mapper) {
    CommandRequest commandRequest = CommandRequestModelFactory.getRequest(commandName)
        .orElseThrow(() -> new IllegalArgumentException("Requête non déclarée dans le factory"));
    return commandRequest.apply(object, mapper);
  }

}
