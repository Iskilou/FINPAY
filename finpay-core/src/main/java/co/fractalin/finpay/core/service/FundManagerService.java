package co.fractalin.finpay.core.service;

import co.fractalin.finpay.commons.dto.request.CreateFundManagerRequest;
import co.fractalin.finpay.commons.dto.request.UpdateFundManagerRequest;
import co.fractalin.finpay.commons.exception.NotFoundException;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import co.fractalin.finpay.commons.mapper.FundManagerToFundManagerDtoMapper;
import co.fractalin.finpay.core.repository.FundManagerRepository;
import co.fractalin.finpay.model.entity.FundManager;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Gestionnaire des FundManagers pour l'API
 */
@Service
@RequiredArgsConstructor
public class FundManagerService {

  private final FundManagerRepository fundManagerRepository;
  private final FundManagerToFundManagerDtoMapper fundManagerMapper;

  /**
   * Lecture de l'institution financière grâce à son id
   *
   * @param id
   * @return
   */
  public FundManager findById(String id) {
    return fundManagerRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(FundManager.class, id));
  }

  /**
   * Enregistrement d'une institution financière
   *
   * @param createFundManagerRequest
   * @return
   */
  @LogArgumentsAndResult
  public FundManager createFundManager(CreateFundManagerRequest createFundManagerRequest) {
    FundManager fundManager = fundManagerMapper.fromFundManagerDTOToFundManager(createFundManagerRequest.getFundManagerDTO());



    return fundManagerRepository.save(fundManager);
  }

  /**
   * Mise à jour d'une institution financière
   *
   * @param updateFundManagerRequest
   * @return
   */
  @LogArgumentsAndResult
  public FundManager updateFundManager(UpdateFundManagerRequest updateFundManagerRequest) {
    FundManager fundManager = this.findById(updateFundManagerRequest.getId());
    if (!Strings.isNullOrEmpty(updateFundManagerRequest.getFundManagerDTO().getName())) {
      fundManager.setName(updateFundManagerRequest.getFundManagerDTO().getName());
    }
    return fundManagerRepository.save(fundManager);
  }
}
