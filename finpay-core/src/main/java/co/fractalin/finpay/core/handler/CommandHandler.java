package co.fractalin.finpay.core.handler;

import co.fractalin.finpay.commons.dto.command.request.CommandRequest;

/**
 * Interface de base pour les {@link CommandHandler} pour gérer les commands entrantes
 */
public interface CommandHandler {

    void execute(final CommandRequest commandRequest);

    String getName();
}
