package co.fractalin.finpay.core.repository;

import co.fractalin.finpay.model.entity.FundManager;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository des fundManagers
 */
public interface FundManagerRepository extends MongoRepository<FundManager, String> {
  FundManager findByName(String name);
}
