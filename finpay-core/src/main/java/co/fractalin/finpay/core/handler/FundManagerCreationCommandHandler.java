package co.fractalin.finpay.core.handler;

import co.fractalin.finpay.commons.dto.command.request.CommandRequest;
import co.fractalin.finpay.commons.dto.command.request.CreateFundManagerCommandRequest;
import co.fractalin.finpay.core.service.FundManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static co.fractalin.finpay.commons.dto.command.Commands.CREATE_FUNDMANAGER;

/**
 * Service permettant de traiter la requête de creation de fundManager
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class FundManagerCreationCommandHandler implements CommandHandler {

  private final FundManagerService service;

  @Override
  public void execute(CommandRequest commandRequest) {
    CreateFundManagerCommandRequest fundManagerRequest = (CreateFundManagerCommandRequest) commandRequest;
    service.createFundManager(fundManagerRequest.getCreateFundManagerRequest());
  }

  @Override
  public String getName() {
    return CREATE_FUNDMANAGER;
  }
}
