package co.fractalin.finpay.core.handler;

import co.fractalin.finpay.commons.dto.command.request.CommandRequest;
import co.fractalin.finpay.commons.dto.command.request.UpdateFundManagerCommandRequest;
import co.fractalin.finpay.core.service.FundManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static co.fractalin.finpay.commons.dto.command.Commands.UPDATE_FUNDMANAGER;

/**
 * Service permettant de traiter la requête de mise à jour  de fundManager
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class FundManagerUpdateCommandHandler implements CommandHandler {

  private final FundManagerService service;

  @Override
  public void execute(CommandRequest commandRequest) {
    UpdateFundManagerCommandRequest fundManagerRequest = (UpdateFundManagerCommandRequest) commandRequest;
    service.updateFundManager(fundManagerRequest.getUpdateFundManagerRequest());
  }

  @Override
  public String getName() {
    return UPDATE_FUNDMANAGER;
  }
}
