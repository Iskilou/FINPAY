///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package co.fractalin.finpay.logs.service;
//
//import com.google.gson.Gson;
//import com.google.gson.internal.LinkedTreeMap;
//import co.fractalin.finpay.model.entity.Log;
//import java.time.Instant;
//import java.util.List;
//import static org.testng.Assert.*;
//import org.testng.annotations.Test;
//
///**
// *
// * @author mboab
// */
//public class LogServiceNGTest {
//
//    @Test
//    public void test() {
//        Gson gson = new Gson();
//        String log_message = "{\n"
//                + "  \"thread\" : \"restartedMain\",\n"
//                + "  \"level\" : \"INFO\",\n"
//                + "  \"loggerName\" : \"co.fractalin.finpay.gateway.PcnGatewayApplication\",\n"
//                + "  \"message\" : \"Started PcnGatewayApplication in 32.7 seconds (JVM running for 36.386)\",\n"
//                + "  \"endOfBatch\" : false,\n"
//                + "  \"loggerFqcn\" : \"org.apache.commons.logging.impl.SLF4JLocationAwareLog\",\n"
//                + "  \"instant\" : {\n"
//                + "    \"epochSecond\" : 1587380071,\n"
//                + "    \"nanoOfSecond\" : 741000000\n"
//                + "  },\n"
//                + "  \"threadId\" : 12,\n"
//                + "  \"threadPriority\" : 5\n"
//                + "}";
//        LinkedTreeMap result = gson.fromJson(log_message, LinkedTreeMap.class);
//        LinkedTreeMap instantMap = (LinkedTreeMap) result.get("instant");
//        Instant instant = Instant.ofEpochSecond(((Double) instantMap.get("epochSecond")).longValue(), ((Double) instantMap.get("nanoOfSecond")).longValue());
//        Log logs = gson.fromJson(log_message, Log.class);
//        logs.setInstant(instant);
//        System.out.println(gson.toJson(logs));
//
//    }
//
////    /**
////     * Test of findById method, of class LogService.
////     */
////    @Test
////    public void testFindById() {
////        System.out.println("findById");
////        String id = "";
////        LogService instance = null;
////        Log expResult = null;
////        Log result = instance.findById(id);
////        assertEquals(result, expResult);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of getAll method, of class LogService.
////     */
////    @Test
////    public void testGetAll() {
////        System.out.println("getAll");
////        LogService instance = null;
////        List expResult = null;
////        List result = instance.getAll();
////        assertEquals(result, expResult);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of createLog method, of class LogService.
////     */
////    @Test
////    public void testCreateLog() {
////        System.out.println("createLog");
////        Log logs = null;
////        LogService instance = null;
////        instance.createLog(logs);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of deleteLog method, of class LogService.
////     */
////    @Test
////    public void testDeleteLog() {
////        System.out.println("deleteLog");
////        Log logs = null;
////        LogService instance = null;
////        instance.deleteLog(logs);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
//}
