package co.fractalin.finpay.logs.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.fractalin.finpay.logs.service.LogService;

/**
 * Contrôleur des webservices de Logs
 */
@CrossOrigin(origins = "*")
@Api(tags = "Gestionnaire de logs")
@RestController
@RequestMapping("/logs")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LogController {

    private final LogService logService;

    /**
     * Méthode permettant d'afficher les logs
     *
     * @return
     */
    @GetMapping
    @ApiOperation("Get all logs")
    public ResponseEntity<String> findAll() {
        String logs = logService.getAll();
        return new ResponseEntity<>(logs, HttpStatus.OK);
    }
}
