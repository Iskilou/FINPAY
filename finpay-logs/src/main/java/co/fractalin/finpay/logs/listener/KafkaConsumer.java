package co.fractalin.finpay.logs.listener;

import co.fractalin.finpay.logs.service.LogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumer {

    private final LogService service;

    @KafkaListener(topics = "pcn-logs", groupId = "group_id")
    public void consume(String message) {
        service.appendLog(message);
    }
}
