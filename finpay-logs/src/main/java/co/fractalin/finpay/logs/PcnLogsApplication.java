package co.fractalin.finpay.logs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(exclude = {
    org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
    org.springframework.boot.actuate.autoconfigure.web.servlet.ServletManagementContextAutoConfiguration.class})
@EnableDiscoveryClient
@Slf4j
public class PcnLogsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcnLogsApplication.class, args);
    }
}
