package co.fractalin.finpay.logs.service;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Gestionnaire des Logs pour l'API PCN
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class LogService {

  private final String fileName = "pcn-logs.log";

  public void appendLog(String message) {
    BufferedWriter writer = null;
    try {
      new FileOutputStream(fileName, true).close();
      writer = new BufferedWriter(new FileWriter(fileName, true));
      writer.append(message);
      writer.close();
    } catch (FileNotFoundException ex) {
      log.error("", ex);
    } catch (IOException ex) {
      log.error("", ex);
    } finally {
      try {
        writer.close();
      } catch (IOException ex) {
        log.error("", ex);
      }
    }
  }

  public String getAll() {
    try {
      log.info("Lecture de tous les logs");
      Charset charset = Charset.forName("UTF-8");
      return readFile(fileName, charset);
    } catch (IOException ex) {
      log.error("", ex);
      return null;
    }
  }

  static String readFile(String path, Charset encoding)
          throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

}
