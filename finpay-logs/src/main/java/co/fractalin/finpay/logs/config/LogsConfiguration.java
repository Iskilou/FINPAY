package co.fractalin.finpay.logs.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import co.fractalin.finpay.commons.config.CommonConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static co.fractalin.finpay.logs.config.LogsConfiguration.COMMON_PACKAGE;
import static co.fractalin.finpay.logs.config.LogsConfiguration.WEB_COMMON_PACKAGE;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Configuration des pages et JsonView pour la logs
 */
@Configuration
@Import(CommonConfig.class)
@ComponentScan(basePackages = {COMMON_PACKAGE, WEB_COMMON_PACKAGE})
public class LogsConfiguration extends WebConfig {

    static final String COMMON_PACKAGE = "co.fractalin.finpay.commons";
    static final String WEB_COMMON_PACKAGE = "co.fractalin.finpay.web.commons";

    @Bean
    @Primary
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        return builder.modulesToInstall(new JavaTimeModule());
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        return objectMapper;
    }
}
