package co.fractalin.finpay.logs.config;

import co.fractalin.finpay.commons.swagger.doc.ApiDocResponses;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocResponses.FORBIDDEN_ACTION;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocResponses.UNAUTHENTICATED_USER;
import static java.util.Arrays.asList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

/**
 * Configuration de Swagger utilisée commune à l'api publique et back-office
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

    private static final String API_KEY_NAME = "Authorization";

    public static Docket makeDocket(String base) {
        return new Docket(SWAGGER_2)
                .useDefaultResponseMessages(false)
                .globalResponseMessage(GET, createResponseMessages())
                .globalResponseMessage(PUT, createResponseMessages())
                .globalResponseMessage(POST, createResponseMessages())
                .globalResponseMessage(DELETE, createResponseMessages())
                .select()
                .apis(basePackage(base))
                .paths(any())
                .build()
                .directModelSubstitute(LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(ZonedDateTime.class, java.util.Date.class)
                .directModelSubstitute(LocalDateTime.class, java.util.Date.class)
                .securitySchemes(newArrayList(apiKey()));
    }

    private static ApiKey apiKey() {
        return new ApiKey(API_KEY_NAME, "api_key", "header");
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .clientId("Phoenix-client-id")
                .clientSecret("Phoenix-client-secret")
                .realm("Phoenix-realm")
                .scopeSeparator(",")
                .build();
    }

    private static ResponseMessage createResponseMessage(int code, String message) {
        return new ResponseMessageBuilder()
                .code(code)
                .message(message)
                .build();
    }

    private static ResponseMessage createForbiddenActionResponseMessage() {
        return createResponseMessage(FORBIDDEN.value(), FORBIDDEN_ACTION);
    }

    private static ResponseMessage createNotFoundResponseMessage() {
        return createResponseMessage(NOT_FOUND.value(), ApiDocResponses.NOT_FOUND);
    }

    private static ResponseMessage createUnauthorizedUserResponseMessage() {
        return createResponseMessage(UNAUTHORIZED.value(), UNAUTHENTICATED_USER);
    }

    private static ResponseMessage createBadRequestResponseMessage() {
        return createResponseMessage(BAD_REQUEST.value(), ApiDocResponses.BAD_REQUEST);
    }

    private static List<ResponseMessage> createResponseMessages() {
        return asList(
                createNotFoundResponseMessage(),
                createForbiddenActionResponseMessage(),
                createUnauthorizedUserResponseMessage(),
                createBadRequestResponseMessage());
    }
}
