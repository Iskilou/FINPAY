package co.fractalin.finpay.logs.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import co.fractalin.finpay.commons.common.view.DefaultView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static lombok.AccessLevel.PRIVATE;

/**
 * Implémentation de {@code Page} compatible avec la désérialisation Jackson
 */
@NoArgsConstructor(access = PRIVATE)
final class PageResponse<T> implements Page<T> {

    private static final String NOT_SUPPORTED_YET = "Not supported yet.";

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private int number;

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private int size;

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private int totalPages;

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private int numberOfElements;

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private long totalElements;

    @Getter(onMethod = @__(
            @JsonView(DefaultView.class)))
    private List<T> content;

    @Override
    @JsonView(DefaultView.class)
    public boolean isFirst() {
        return number == 0;
    }

    @Override
    @JsonView(DefaultView.class)
    public boolean isLast() {
        return number * size + numberOfElements == totalElements;
    }

    @Override
    @JsonView(DefaultView.class)
    public Sort getSort() {
        return null;
    }

    @Override
    public Pageable nextPageable() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    @Override
    public Pageable previousPageable() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    @Override
    @JsonIgnore
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    @Override
    @JsonIgnore
    public boolean hasContent() {
        return content != null && !content.isEmpty();
    }

    @Override
    @JsonIgnore
    public boolean hasPrevious() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    @Override
    @JsonIgnore
    public boolean hasNext() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    @Override
    public <U> Page<U> map(Function<? super T, ? extends U> function) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }
}
