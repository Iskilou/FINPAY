package co.fractalin.finpay.gateway.controller;

import co.fractalin.finpay.commons.dto.security.AuthenticationRequest;
import co.fractalin.finpay.commons.dto.security.AuthenticationResponse;
import co.fractalin.finpay.gateway.service.AuthenticationService;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.AUTHENTICATION_REQUEST;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.AUTHENTICATE;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.REFRESH_AUTHENTICATION;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocResponses.AUTHENTICATION_TOKEN;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocTags.AUTHENTICATION;
import static co.fractalin.finpay.web.commons.security.WebTokenService.deleteCookie;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Contrôleur des webservices de l'authentification
 */
@CrossOrigin(origins = "${application.cross-origin}")
@Api(tags = AUTHENTICATION)
@RestController
@RequestMapping(AuthenticationController.ROUTE_AUTHENTICATION)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthenticationController {
  public static final String ROUTE_AUTHENTICATION = "/login";
  public static final String ROUTE_AUTHENTICATION_REFRESH = "/refresh";

  private final AuthenticationService authenticationService;

  /**
   * Méthode permettant d'authentifier et de renvoyer le token
   */
  @ApiOperation(AUTHENTICATE)
  @ApiResponses(@ApiResponse(code = 200, message = AUTHENTICATION_TOKEN))
  @RequestMapping(method = POST)
  public AuthenticationResponse authenticate(@ApiParam(AUTHENTICATION_REQUEST) @RequestBody AuthenticationRequest request,
                                             HttpServletResponse response) {
    return authenticationService.authenticate(request, response);
  }

  /**
   * Rafraîchi le jeton
   */
  @ApiOperation(REFRESH_AUTHENTICATION)
  @ApiResponses(@ApiResponse(code = 200, message = AUTHENTICATION_TOKEN))
  @GetMapping(path = ROUTE_AUTHENTICATION_REFRESH)
  public AuthenticationResponse refreshAuthentication(HttpServletRequest request, HttpServletResponse response) {
    return authenticationService.refreshAuthentication(request, response);
  }

  @ApiIgnore
  @GetMapping
  @PreAuthorize("isAuthenticated()")
  public Claims getCurrentUser(@AuthenticationPrincipal FinpayParsedToken token) {
    return token.getClaims();
  }

  @ApiIgnore
  @GetMapping(path = "/logout")
  @PreAuthorize("isAuthenticated()")
  public void logout(HttpServletResponse response) {
    deleteCookie(response);
  }
}
