package co.fractalin.finpay.gateway.config;

import co.fractalin.finpay.gateway.security.FinpayPublicRoutesMatcher;
import co.fractalin.finpay.web.commons.security.AuthenticationTokenFilter;
import co.fractalin.finpay.web.commons.security.EntryPointUnauthorizedHandler;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.security.config.BeanIds.AUTHENTICATION_MANAGER;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final UserDetailsService userDetailsService;
  private final AuthenticationTokenFilter<FinpayParsedToken> tokenFilter;
  private final FinpayPublicRoutesMatcher publicRoutesMatcher;

  @Override
  @Bean(name = AUTHENTICATION_MANAGER)
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
        .userDetailsService(userDetailsService)
        .passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {

    httpSecurity.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);

    httpSecurity
        .authorizeRequests()
        .requestMatchers(publicRoutesMatcher).permitAll()
        .anyRequest().authenticated()
        // renvoyer 401 sur les ws protégés
        .and().exceptionHandling().authenticationEntryPoint(new EntryPointUnauthorizedHandler())
        .and().sessionManagement()
        .sessionCreationPolicy(STATELESS)
        .and().csrf().disable();
    httpSecurity.cors();
  }
}
