package co.fractalin.finpay.gateway.broker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import co.fractalin.finpay.gateway.command.Command;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Agent de publication de commande
 */
@Slf4j
@Component
public class CommandPublisher {

  private final String topic;
  private final ObjectMapper objectMapper;
  private final KafkaProducer<String, String> commandProducer;

  public CommandPublisher(@Value("${application.kafka.command.topic}") String topic,
                          ObjectMapper objectMapper,
                          KafkaProducer<String, String> commandProducer) {
    this.topic = topic;
    this.objectMapper = objectMapper;
    this.commandProducer = commandProducer;
  }

  public void publish(final Command command) {
    log.info("Publication de la commande : {}", command.getCommandName());
    try {
      this.commandProducer.send(new ProducerRecord<>(
          this.topic, command.getCommandName(), this.objectMapper.writeValueAsString(command.getRequest())));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  public void publish(final Set<Command> commandSet) {
    commandSet.forEach(this::publish);
  }
}
