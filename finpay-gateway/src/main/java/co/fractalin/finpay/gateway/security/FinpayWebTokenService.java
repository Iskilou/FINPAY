package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.gateway.repository.OrganizationRepository;
import co.fractalin.finpay.model.enumeration.Role;
import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.web.commons.security.WebTokenService;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import co.fractalin.finpay.web.commons.security.model.FinpayUserWithDetails;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

import static co.fractalin.finpay.web.commons.security.model.FinpayParsedToken.ORGANIZATION;
import static co.fractalin.finpay.web.commons.security.model.FinpayParsedToken.ORGANIZATION_NAME;
import static co.fractalin.finpay.web.commons.security.model.FinpayParsedToken.ROLE;
import static co.fractalin.finpay.web.commons.security.model.FinpayParsedToken.USERNAME;
import static io.jsonwebtoken.Claims.SUBJECT;

/**
 * Service pour la manipulation des jetons
 */
@Service
public class FinpayWebTokenService extends WebTokenService<FinpayParsedToken, FinpayUserWithDetails> {


  private final OrganizationRepository organizationRepository;

  public FinpayWebTokenService(
      @Value("${application.web.token.secret}") String secret,
      @Value("${application.web.token.duration}") long expirationDuration,
      @Value("${application.web.token.auth-header}") String authHeader,
      @Value("${application.web.token.auth-prefix}") String authPrefix,

      OrganizationRepository organizationRepository) {
    super(secret, expirationDuration, authHeader, authPrefix);

    this.organizationRepository = organizationRepository;
  }

  public String generateToken(FinpayUserWithDetails user) {
    return user.getUser().getRole().equals(Role.BASIC) ?
        generateForUser(user) : generateForAdmin(user);
  }

  private String generateForAdmin(FinpayUserWithDetails user) {
    User finpayUser = user.getUser();
    ImmutableMap.Builder<String, Object> claimsPropertiesBuilder = ImmutableMap.builder();

    switch (finpayUser.getRole()) {
      case SUPER_ADMIN:
        generateForSuperAdmin(finpayUser, claimsPropertiesBuilder);
        break;
    }

    return generate(user, claimsPropertiesBuilder.build());
  }

  private void generateForSuperAdmin(User finpayUser, ImmutableMap.Builder<String, Object> claimsPropertiesBuilder) {
    claimsPropertiesBuilder.put(ROLE, finpayUser.getRole());
  }

  private String generateForUser(FinpayUserWithDetails user) {

    User finpayUser = user.getUser();
    ImmutableMap.Builder<String, Object> claimsPropertiesBuilder = ImmutableMap.builder();

    claimsPropertiesBuilder.put(USERNAME, finpayUser.getUsername());
    claimsPropertiesBuilder.put(ORGANIZATION, finpayUser.getOrganizationId());
    claimsPropertiesBuilder.put(ROLE, finpayUser.getRole());

    return generate(user, claimsPropertiesBuilder.build());
  }



  private String generate(FinpayUserWithDetails user, Map<String, Object> services) {
    return generateTokenFromClaims(ImmutableMap.<String, Object>builder()
        .put(SUBJECT, user.getUsername())
        .putAll(services)
        .build());
  }

  @Override
  public String refresh(FinpayParsedToken parsedToken,  FinpayUserWithDetails user) {
    if (!Objects.equals(parsedToken.getUsername(), user.getUsername())) {
      throw new InsufficientAuthenticationException(INSUFFICIENT_AUTHENTICATION_EXCEPTION_MESSAGE);
    }

    return generateTokenFromClaims(parsedToken.getClaims());
  }

  @Override
  public FinpayParsedToken parse(String token) {
    return new FinpayParsedToken(token, getSecret(), getConversionService());
  }

  public Authentication emptyAuthentication() {
    return new FinpayJwtAuthentication(new FinpayParsedToken(getConversionService()));
  }

  @Override
  public Authentication getAuthentication(FinpayParsedToken token) {
    return new FinpayJwtAuthentication(token);
  }
}
