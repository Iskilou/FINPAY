package co.fractalin.finpay.gateway.initializer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Profile("initializer")
@Component("initializer")
@RequiredArgsConstructor
public class DataInitializer {







}
