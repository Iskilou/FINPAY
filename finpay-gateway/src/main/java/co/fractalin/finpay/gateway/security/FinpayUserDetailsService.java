package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.gateway.repository.UserRepository;
import co.fractalin.finpay.web.commons.security.model.FinpayUserWithDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Service associé à la recherche des utilisteurs
 */
@Service
@RequiredArgsConstructor
public class FinpayUserDetailsService implements UserDetailsService {

  private final UserRepository repository;

  @Override
  public FinpayUserWithDetails loadUserByUsername(String username) throws UsernameNotFoundException {
   User user =new User();
    return new FinpayUserWithDetails(user);
  }
}
