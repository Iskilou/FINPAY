package co.fractalin.finpay.gateway.initializer.messaging;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface StreamProcessor {
  String INPUT = "input";

  @Input(StreamProcessor.INPUT)
  KStream input();

}
