package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.commons.dto.UserDto;
import co.fractalin.finpay.commons.dto.security.PasswordResetRequest;
import co.fractalin.finpay.commons.dto.security.PasswordUpdateRequest;
import co.fractalin.finpay.commons.exception.ForbiddenActionException;
import co.fractalin.finpay.commons.exception.InvalidPasswordException;
import co.fractalin.finpay.commons.exception.InvalidTokenException;
import co.fractalin.finpay.commons.exception.MultipleUsersFoundWithEmailException;
import co.fractalin.finpay.commons.exception.NotActiveAccountException;
import co.fractalin.finpay.commons.exception.RequiredPasswordException;
import co.fractalin.finpay.commons.exception.UniquenessConstraintException;
import co.fractalin.finpay.commons.service.ValidationService;
import co.fractalin.finpay.gateway.repository.UserRepository;
import co.fractalin.finpay.gateway.service.UserService;
import co.fractalin.finpay.gateway.service.UserTokenService;
import co.fractalin.finpay.model.enumeration.Role;
import co.fractalin.finpay.model.enumeration.UserStatus;
import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.model.security.UserToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static co.fractalin.finpay.commons.utils.DatabaseUtils.generateId;
import java.util.Optional;

import static co.fractalin.finpay.commons.exception.InvalidPasswordException.OLD_PASSWORD_LABEL;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocConstants.USERNAME;
import static co.fractalin.finpay.model.enumeration.UserStatus.ACTIVATED;
import static co.fractalin.finpay.model.enumeration.UserStatus.INACTIVE;
import static co.fractalin.finpay.model.enumeration.UserStatus.TO_BE_ACTIVATED;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Service associé au compte utilisateur
 */
@Slf4j
@Service
@Transactional
public class FinpayUserService {

    private final UserRepository repository;
    private final ValidationService validationService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserTokenService userTokenService;
    private final UserService userService;
    private final long tokenDuration;

    public FinpayUserService(UserRepository repository,
                             ValidationService validationService,
                             BCryptPasswordEncoder passwordEncoder,
                             UserTokenService userTokenService,
                             UserService userService,
                             @Value("${application.web.token.duration}") long tokenDuration) {
        this.repository = repository;
        this.validationService = validationService;
        this.passwordEncoder = passwordEncoder;
        this.userTokenService = userTokenService;
        this.userService = userService;
        this.tokenDuration = tokenDuration;
    }

    @Transactional
    public User create(UserDto userDTO, String organizationId, Role role) {

        if (isUsernameAlreadyUsed(userDTO.getUsername())) {
            throw new UniquenessConstraintException(USERNAME);
        }

        User user = User.builder()
                .id(generateId())
                .status(TO_BE_ACTIVATED)
                .passwordHash(passwordEncoder.encode(userDTO.getPassword()))
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .role(role)
                .organizationId(organizationId)
                .build();

        return repository.save(user);
    }

    public boolean checkUserExistance(String email) {
        return isUsernameAlreadyUsed(email);
    }

    private boolean isUsernameAlreadyUsed(String username) {
        return repository.findByUsername(username).isPresent();
    }

    public Optional<User> findByUsername(String email) {
        if (repository.countByUsername(email) > 1) {
            throw new MultipleUsersFoundWithEmailException();
        }
        return repository.findByUsername(email);
    }

    @Transactional
    public User activate(UserToken token, User user, String password) {
        if (user.getStatus() != TO_BE_ACTIVATED) {
            throw new ForbiddenActionException();
        }

        if (isBlank(password)) {
            if (user.getPasswordHash() == null) {
                throw new RequiredPasswordException();
            }
        } else {
            updatePassword(user, password);
        }

        userService.activate(user);
        userTokenService.delete(token);

        return user;
    }

    @Transactional
    public User updatePassword(User user, String password) {
        user.setPassword(password);
        validationService.validate(user);

        user.setPasswordHash(passwordEncoder.encode(password));
        return repository.save(user);
    }

    @Transactional
    public User updatePasswordWithVerification(User user, PasswordUpdateRequest request) {
        validationService.validate(request);

        if (!passwordEncoder.matches(request.getOldPassword(), user.getPasswordHash())) {
            throw new InvalidPasswordException(OLD_PASSWORD_LABEL);
        }

        return updatePassword(user, request.getNewPassword());
    }

    @Transactional
    public User resetPassword(PasswordResetRequest request) {
        validationService.validate(request);

        UserToken token = userTokenService.findByValue(request.getToken());

        if (!token.isValid(tokenDuration)) {
            throw new InvalidTokenException(token.getValue());
        }

        Optional<User> optionalUser = repository.findById(token.getUserId());
        optionalUser.ifPresent(user -> user = updatePassword(user, request.getPassword()));
        userTokenService.delete(token);

        return optionalUser.orElse(null);
    }

    public void verifyCanAuthenticate(User user) {
        if (user.getStatus() != ACTIVATED) {
            throw new NotActiveAccountException(user.getId());
        }
    }

    public Optional<User> findById(String id) {
        return repository.findById(id);
    }

    public User getById(String id) {
        try {
            return repository.findById(id).get();
        } catch (Exception ex) {
            return null;
        }
    }

    @Transactional(readOnly = false)
    public User createFinpayAdmin(UserDto userDTO, String organizationId, UserStatus status) {

        if (isUsernameAlreadyUsed(userDTO.getUsername())) {
            throw new UniquenessConstraintException(USERNAME);
        }
        User user = User.builder()
                .id(generateId())
                .status(status)
                .passwordHash(passwordEncoder.encode(userDTO.getPassword()))
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .role(Role.BASIC)
                .organizationId(organizationId)
                .build();

        return repository.save(user);
    }



    @Transactional
    public User changeUserStatus(User user, UserStatus userStatus) {
        if (userStatus.equals(UserStatus.ACTIVATED)) {
            if (user.getStatus() == ACTIVATED) {
                log.error("User {} is already active", user.getUsername());
                throw new ForbiddenActionException();
            }
            userService.activate(user);
            return user;
        } else if (userStatus.equals(UserStatus.INACTIVE)) {
            if (user.getStatus() == INACTIVE) {
                log.error("User {} is already inactive", user.getUsername());
                throw new ForbiddenActionException();
            }
            userService.disactivate(user);
            return user;
        }
        throw new ForbiddenActionException();
    }

}
