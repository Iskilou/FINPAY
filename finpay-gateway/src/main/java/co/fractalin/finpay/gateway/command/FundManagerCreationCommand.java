package co.fractalin.finpay.gateway.command;

import co.fractalin.finpay.commons.dto.request.CreateFundManagerRequest;
import co.fractalin.finpay.commons.dto.command.request.CreateFundManagerCommandRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import static co.fractalin.finpay.commons.dto.command.Commands.CREATE_FUNDMANAGER;

/**
 * Commande de creation de fundManager
 */
public class FundManagerCreationCommand extends Command {

    @Setter
    @Getter
    private CreateFundManagerRequest createFundManagerRequest;

    @Builder
    public FundManagerCreationCommand(CreateFundManagerRequest createFundManagerRequest) {
        this.createFundManagerRequest = createFundManagerRequest;
    }

    @Override
    public String getCommandName() {
        return CREATE_FUNDMANAGER;
    }

    @Override
    public CreateFundManagerCommandRequest getRequest() {
        return CreateFundManagerCommandRequest.builder()
                .createFundManagerRequest(createFundManagerRequest)
                .build();
    }
}
