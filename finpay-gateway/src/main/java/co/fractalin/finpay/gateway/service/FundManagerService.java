package co.fractalin.finpay.gateway.service;

import co.fractalin.finpay.commons.dto.FundManagerDto;
import co.fractalin.finpay.commons.dto.request.CreateFundManagerRequest;
import co.fractalin.finpay.commons.dto.request.UpdateFundManagerRequest;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import co.fractalin.finpay.commons.mapper.FundManagerToFundManagerDtoMapper;
import co.fractalin.finpay.gateway.broker.CommandPublisher;
import co.fractalin.finpay.gateway.command.Command;
import co.fractalin.finpay.gateway.command.FundManagerCreationCommand;
import co.fractalin.finpay.gateway.command.FundManagerUpdateCommand;
import co.fractalin.finpay.gateway.repository.FundManagerRepository;
import co.fractalin.finpay.model.entity.FundManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class FundManagerService {

  private final FundManagerRepository repository;
  private final FundManagerToFundManagerDtoMapper mapper;
  private final CommandPublisher commandPublisher;

  /**
   * Lecture des informations d'une institutions financières à partir de son
   * id
   *
   * @param id
   * @return
   */
  public FundManagerDto findById(String id) {

    return null;
  }

  /**
   * Liste des institutions financières présentent dans le système
   *
   * @return
   */
  @LogArgumentsAndResult
  public List<FundManagerDto> findAll() {
    return null;
  }

  /**
   * Enregistrement d'une institution financière
   *
   * @param createFundManagerRequest
   * @return
   */
  @LogArgumentsAndResult
  public Command save(CreateFundManagerRequest createFundManagerRequest) {
    FundManagerCreationCommand command = FundManagerCreationCommand.builder()
        .createFundManagerRequest(createFundManagerRequest)
        .build();
    commandPublisher.publish(command);
    return command;
  }

  /**
   * Mise à jour d'une institution financière
   *
   * @param createFundManagerRequest
   * @return
   */
  @LogArgumentsAndResult
  public Command update(CreateFundManagerRequest createFundManagerRequest) {
    UpdateFundManagerRequest updateFundManagerRequest = new UpdateFundManagerRequest(createFundManagerRequest.getFundManagerDTO().getId(), createFundManagerRequest.getFundManagerDTO());
    FundManagerUpdateCommand command = FundManagerUpdateCommand.builder()
        .updateFundManagerRequest(updateFundManagerRequest)
        .build();
    commandPublisher.publish(command);
    return command;
  }
  
  private FundManagerDto fundManagerToFundManagerDTO(FundManager fundManager){
      FundManagerDto fundManagerDto = mapper.fromFundManagerToFundManagerDTO(fundManager);
      try {

      } catch (Exception e) {
          log.error("",e);
      }
      return fundManagerDto;
  }
}
