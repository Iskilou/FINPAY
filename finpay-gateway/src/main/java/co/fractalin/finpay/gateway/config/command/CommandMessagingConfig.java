package co.fractalin.finpay.gateway.config.command;

import com.google.gson.Gson;
import co.fractalin.finpay.gateway.config.KafkaProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configurer class to bootstrap the needed artifacts for RabbitMQ
 */
@Slf4j
@Configuration
public class CommandMessagingConfig {

  private final KafkaProperties properties;

  public CommandMessagingConfig(KafkaProperties properties) {
    this.properties = properties;
  }

  @Bean
  public KafkaProducer<String, String> commandProducer() {
    return new KafkaProducer<>(properties.getProducerProps());
  }

  @Bean
  public Gson gson() {
    return new Gson();
  }
}
