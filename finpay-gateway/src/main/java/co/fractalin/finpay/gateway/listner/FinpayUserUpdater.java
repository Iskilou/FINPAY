package co.fractalin.finpay.gateway.listner;


import co.fractalin.finpay.commons.dto.event.CreateUserEvent;
import co.fractalin.finpay.gateway.initializer.messaging.StreamProcessor;
import co.fractalin.finpay.gateway.service.UserService;
import co.fractalin.finpay.model.security.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static co.fractalin.finpay.commons.dto.event.FinpayDomainEventType.USER_CREATION_EVENT;


/**
 * Listner des events de mise à jour des statuts utilisateurs
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FinpayUserUpdater {

  private final UserService userService;

  @StreamListener(StreamProcessor.INPUT)
  public void updateUser(KStream<String, CreateUserEvent> createdUserEvent) {
    createdUserEvent.filter((key, value) -> Objects.equals(key, USER_CREATION_EVENT))
            .foreach((key, value) -> {
              try {
                log.info("key {}", key);
                User user = userService.findById(value.getUserEventPayload().getUserId());
                userService.activate(user);
              } catch (Exception e) {
                log.error("", e);
              }
            });
  }


}
