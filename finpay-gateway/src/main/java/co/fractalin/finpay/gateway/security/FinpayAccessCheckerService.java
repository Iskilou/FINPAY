package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import co.fractalin.finpay.commons.exception.ForbiddenActionException;
import co.fractalin.finpay.model.enumeration.Feature;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;

/**
 * Service associé à la sécurisation par JWT
 */
@Service
@Slf4j
public class FinpayAccessCheckerService {

  public boolean hasFeatures(Feature... features) {
    FinpayParsedToken token = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
            .map(Authentication::getPrincipal)
            .map(FinpayParsedToken.class::cast)
            .orElse(null);
    log.info("Token = {}", token);
    
    return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
            .map(Authentication::getPrincipal)
            .map(FinpayParsedToken.class::cast)
            .map(FinpayParsedToken::getRole)
            .map(role -> role.hasFeatures(features))
            .orElse(false);
  }

  public boolean hasFeature(Feature feature) {
    return hasFeatures(feature);
  }



  public static void checkAccessRightWithOrganizationId(FinpayParsedToken token, UUID organizationId) {
    Optional.ofNullable(token.getOrganization()).ifPresent(tokenOrganizationId -> {
      if (organizationId == null || !Objects.equals(tokenOrganizationId, organizationId)) {
        throw new ForbiddenActionException();
      }
    });
  }
}
