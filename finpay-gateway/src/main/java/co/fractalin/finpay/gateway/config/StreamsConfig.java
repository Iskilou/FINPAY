package co.fractalin.finpay.gateway.config;

import co.fractalin.finpay.gateway.initializer.messaging.StreamProcessor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(StreamProcessor.class)
public class StreamsConfig {
}
