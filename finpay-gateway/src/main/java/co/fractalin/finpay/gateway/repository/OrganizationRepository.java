package co.fractalin.finpay.gateway.repository;

import co.fractalin.finpay.model.entity.Organization;
import co.fractalin.finpay.model.security.User;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface OrganizationRepository extends MongoRepository<Organization, String> {
  @AllowFiltering
  Optional<Organization> findByName(String name);
}
