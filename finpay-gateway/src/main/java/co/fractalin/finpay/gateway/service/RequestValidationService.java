package co.fractalin.finpay.gateway.service;

import co.fractalin.finpay.commons.exception.NotFoundException;
import co.fractalin.finpay.commons.service.ValidationService;
import co.fractalin.finpay.gateway.repository.OrganizationRepository;
import co.fractalin.finpay.model.entity.Organization;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RequestValidationService {

    private final ValidationService validationService;
    private final OrganizationRepository organizationRepository;


    private void validateOrganizationId(String organizationId) {
        if (!organizationRepository.findById(organizationId).isPresent()) {
            throw new  NotFoundException(Organization.class, "");
        }
    }
}
