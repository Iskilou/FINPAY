package co.fractalin.finpay.gateway.repository;

import co.fractalin.finpay.model.security.UserToken;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface TokenRepository extends MongoRepository<UserToken, String> {
  @AllowFiltering
  Optional<UserToken> findByValue(String value);
  @AllowFiltering
  List<UserToken> findByUserId(String userId);
}
