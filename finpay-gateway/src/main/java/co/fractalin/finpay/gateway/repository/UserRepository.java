package co.fractalin.finpay.gateway.repository;

import co.fractalin.finpay.model.security.User;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Repository des utilisateurs
 */
public interface UserRepository extends MongoRepository<User, String> {
  @AllowFiltering
  Optional<User> findByUsername(String username);
  @AllowFiltering
  int countByUsername(String username);
}
