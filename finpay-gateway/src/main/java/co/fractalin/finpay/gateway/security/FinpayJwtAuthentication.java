package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.web.commons.security.JwtAuthentication;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;

import static java.util.Collections.singletonList;

/**
 * Objet qui représente le token d'authentification simplifié
 */
public class FinpayJwtAuthentication extends JwtAuthentication<FinpayParsedToken> {

  public FinpayJwtAuthentication(FinpayParsedToken token) {
    super(token);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return singletonList(new SimpleGrantedAuthority("authenticated-customer"));
  }
}
