package co.fractalin.finpay.gateway.service;

import co.fractalin.finpay.commons.dto.security.AuthenticationRequest;
import co.fractalin.finpay.commons.dto.security.AuthenticationResponse;
import co.fractalin.finpay.gateway.security.FinpayUserDetailsService;
import co.fractalin.finpay.gateway.security.FinpayUserService;
import co.fractalin.finpay.gateway.security.FinpayWebTokenService;
import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import co.fractalin.finpay.web.commons.security.model.FinpayUserWithDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Gestionnaire d'authentification pour l'API PCN
 */
@Service
@RequiredArgsConstructor
public class AuthenticationService {

  private final AuthenticationManager authenticationManager;
  private final FinpayUserService finpayUserService;
  private final FinpayWebTokenService finpayWebTokenService;
  private final FinpayUserDetailsService userDetailsService;



  public AuthenticationResponse authenticate(AuthenticationRequest request, HttpServletResponse response) {
    String username = request.getUsername();
    Optional<User> optionalUser = finpayUserService.findByUsername(username);

    optionalUser.ifPresent(finpayUserService::verifyCanAuthenticate);

    Authentication authentication;
    authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
        optionalUser.map(User::getUsername).orElse(username),
        request.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);

    FinpayUserWithDetails authenticatedUser = (FinpayUserWithDetails) authentication.getPrincipal();

    String token = finpayWebTokenService.generateToken(authenticatedUser);

    finpayWebTokenService.addTokenToResponse(response, token);

    return new AuthenticationResponse(token);
  }

  public AuthenticationResponse refreshAuthentication(HttpServletRequest request, HttpServletResponse response) {
    String token = finpayWebTokenService.getToken(request);
    FinpayParsedToken parsedToken = finpayWebTokenService.parse(token);
    String username = parsedToken.getUsername();

    FinpayUserWithDetails userWithDetails = userDetailsService.loadUserByUsername(username);

    String newToken = finpayWebTokenService.refresh(parsedToken, userWithDetails);
    finpayWebTokenService.addTokenToResponse(response, newToken);

    return new AuthenticationResponse(newToken);
  }
}
