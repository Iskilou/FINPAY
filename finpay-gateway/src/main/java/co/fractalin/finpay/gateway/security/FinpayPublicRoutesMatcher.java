package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.web.commons.security.PublicRoutesMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import static co.fractalin.finpay.gateway.controller.AuthenticationController.ROUTE_AUTHENTICATION;


/**
 * Matcher pour les routes publiques qui ne sont pas sujettes au filtre sur le token
 */
@Component
public class FinpayPublicRoutesMatcher extends PublicRoutesMatcher {

  FinpayPublicRoutesMatcher() {
    super(
        new AntPathRequestMatcher("/actuator/**"),
        new AntPathRequestMatcher(ROUTE_AUTHENTICATION, POST),
        new AntPathRequestMatcher("/password_reset_email", POST),
        new AntPathRequestMatcher("/password_reset", PUT));
  }
}