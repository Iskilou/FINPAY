package co.fractalin.finpay.gateway.security;

import co.fractalin.finpay.commons.exception.ForbiddenActionException;
import co.fractalin.finpay.model.entity.Organization;
import co.fractalin.finpay.model.security.User;
import co.fractalin.finpay.web.commons.security.model.FinpayParsedToken;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Service associé à la sécurisation par json web token
 * /!\ pas une classe Utils car PreAuthorize cherche un bean
 */
@Service
public class FinpaySecurityService {

  public static void checkAccessRight(FinpayParsedToken token, User user) {
    checkAccessRightWithUser(token, user);
  }



  public static void checkAccessRight(FinpayParsedToken token, Organization organization) {
    checkAccessRightWithOrganizationId(token, organization.getId());
  }


  public static void checkAccessRightWithOrganizationId(FinpayParsedToken token, String organizationId) {
    if (!Objects.equals(token.getOrganization(), organizationId)) {
      throw new ForbiddenActionException();
    }
  }

  private static void checkAccessRightWithUser(FinpayParsedToken token, User user) {
    if (!Objects.equals(token.getFirstname(), user.getUsername()) ) {
      throw new ForbiddenActionException();
    }
  }

}
