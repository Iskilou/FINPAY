package co.fractalin.finpay.gateway.query;

import org.springframework.data.mongodb.core.query.*;
import org.springframework.data.repository.Repository;
/**
 * Repository générique permettant servant de base pour la lecture des entites
 * @param <T> entité
 * @param <ID> classe d'identifiant de l'entité
 */
public abstract class MongoCoreRepository<T, ID> implements Repository<T, ID> {
  private final Query query ;
  private final Criteria criteria;

  public  MongoCoreRepository(final Query query, final Criteria criteria) {
    this.query = query;
    this.criteria = criteria;
  }

}
