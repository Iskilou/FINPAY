package co.fractalin.finpay.gateway.repository;

import co.fractalin.finpay.model.entity.FundManager;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FundManagerRepository extends MongoRepository<FundManager, String>{
  Optional<FundManager> findById(String id);
}
