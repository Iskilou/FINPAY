package co.fractalin.finpay.gateway.command;

import co.fractalin.finpay.commons.dto.request.UpdateFundManagerRequest;
import co.fractalin.finpay.commons.dto.command.request.UpdateFundManagerCommandRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import static co.fractalin.finpay.commons.dto.command.Commands.UPDATE_FUNDMANAGER;

/**
 * Commande de creation de fundManager
 */
public class FundManagerUpdateCommand extends Command {

    @Setter
    @Getter
    private UpdateFundManagerRequest updateFundManagerRequest;

    @Builder
    public FundManagerUpdateCommand(UpdateFundManagerRequest updateFundManagerRequest) {
        this.updateFundManagerRequest = updateFundManagerRequest;
    }

    @Override
    public String getCommandName() {
        return UPDATE_FUNDMANAGER;
    }

    @Override
    public UpdateFundManagerCommandRequest getRequest() {
        return UpdateFundManagerCommandRequest.builder()
                .updateFundManagerRequest(updateFundManagerRequest)
                .build();
    }
}
