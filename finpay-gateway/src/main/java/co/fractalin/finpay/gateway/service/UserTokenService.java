package co.fractalin.finpay.gateway.service;

import co.fractalin.finpay.commons.exception.UnknownTokenException;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import co.fractalin.finpay.gateway.repository.TokenRepository;
import co.fractalin.finpay.model.security.UserToken;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserTokenService {

  private final TokenRepository repository;

  @LogArgumentsAndResult
  @Transactional(readOnly = false)
  public UserToken create(String userId) {
    return repository.save(new UserToken(userId));
  }

  @Transactional(readOnly = false)
  public void delete(UserToken token) {
    repository.delete(token);
  }

  public UserToken findByValue(String value) {
    return repository.findByValue(value)
        .orElseThrow(() -> new UnknownTokenException(value));
  }

  @LogArgumentsAndResult
  @Transactional(readOnly = false)
  public void deleteForUserId(String userId) {
    repository.findByUserId(userId).forEach(repository::delete);
  }
}
