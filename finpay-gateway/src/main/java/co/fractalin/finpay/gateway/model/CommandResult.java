package co.fractalin.finpay.gateway.model;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

/**
 * Reponse générique suite à l'exécution d'une commande
 */
@Data
@Builder
public class CommandResult {
  private UUID id;
  private String message;
}
