package co.fractalin.finpay.gateway.controller;

import co.fractalin.finpay.commons.dto.FundManagerDto;
import co.fractalin.finpay.commons.dto.request.CreateFundManagerRequest;
import co.fractalin.finpay.gateway.command.Command;
import co.fractalin.finpay.gateway.model.CommandResult;
import co.fractalin.finpay.gateway.service.FundManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.CREATE_FUNDMANAGER;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.CREATE_FUNDMANAGER_REQUEST;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.FIND_FUNDMANAGER;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.FIND_FUNDMANAGERS;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.FUNDMANAGER_ID;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.UPDATE_FUNDMANAGER;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocOperations.UPDATE_FUNDMANAGER_REQUEST;
import static co.fractalin.finpay.commons.swagger.doc.ApiDocTags.FUNDMANAGERS;

/**
 * Contrôleur des webservices FundManager
 */
@CrossOrigin(origins = "${application.cross-origin}")
@Api(tags = FUNDMANAGERS)
@RestController
@RequestMapping("/fundManagers")
@PreAuthorize("denyAll")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class FundManagerController {

  private final FundManagerService fundManagerService;

  /**
   * Méthode permettant d'afficher les institutions financières
   *
   * @param pageable
   * @return
   */
  @GetMapping("search")
  @ApiOperation(FIND_FUNDMANAGERS)
  @PreAuthorize("@pcnAccessCheckerService.hasFeature('FIND_ALL_FUND_MANAGERS')")
  public Page<FundManagerDto> findAll(Pageable pageable) {

    List<FundManagerDto> fundManagers = fundManagerService.findAll();

    return new PageImpl<>(fundManagers, pageable, fundManagers.size());
  }

  /**
   * Méthode permettant lire une institution financière
   *
   * @param id
   * @return
   */
  @GetMapping("{id}")
  @ApiOperation(FIND_FUNDMANAGER)
  @PreAuthorize("@pcnAccessCheckerService.hasFeature('FIND_FUND_MANAGER')")
  public ResponseEntity<FundManagerDto> read(@ApiParam(FUNDMANAGER_ID) @PathVariable(name = "id") String id) {
    FundManagerDto fundManagerDTO = fundManagerService.findById(id);
    return new ResponseEntity<>(fundManagerDTO, HttpStatus.OK);
  }

  @PostMapping
  @ApiOperation(CREATE_FUNDMANAGER)
  @PreAuthorize("@pcnAccessCheckerService.hasFeature('ADD_FUND_MANAGER')")
  public ResponseEntity<CommandResult> createFundManager(@ApiParam(CREATE_FUNDMANAGER_REQUEST) @RequestBody CreateFundManagerRequest createFundManagerRequest) {
    Command command = fundManagerService.save(createFundManagerRequest);
    Instant end = Instant.now();
    return new ResponseEntity<>(
            CommandResult
                    .builder()
                    .id(command.getId())
                    .message("La commande est prise en compte !")
                    .build(),
            HttpStatus.OK);
  }

  @PutMapping
  @ApiOperation(UPDATE_FUNDMANAGER)
  @PreAuthorize("@pcnAccessCheckerService.hasFeature('UPDATE_FUND_MANAGER')")
  public ResponseEntity<CommandResult> updateFundManager(@ApiParam(UPDATE_FUNDMANAGER_REQUEST) @RequestBody CreateFundManagerRequest createFundManagerRequest) {

    Command command = fundManagerService.update(createFundManagerRequest);

    return new ResponseEntity<>(
            CommandResult
                    .builder()
                    .id(command.getId())
                    .message("La commande est prise en compte !")
                    .build(),
            HttpStatus.OK);
  }
}
