package co.fractalin.finpay.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication(exclude = {
    org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
    org.springframework.boot.actuate.autoconfigure.web.servlet.ServletManagementContextAutoConfiguration.class})
@EnableDiscoveryClient
public class FinpayGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(FinpayGatewayApplication.class, args);
  }
}

