package co.fractalin.finpay.gateway.config;

import co.fractalin.finpay.commons.config.CommonConfig;
import co.fractalin.finpay.web.commons.config.SwaggerConfiguration;
import co.fractalin.finpay.web.commons.config.WebConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static co.fractalin.finpay.gateway.config.GatewayConfiguration.COMMON_PACKAGE;
import static co.fractalin.finpay.gateway.config.GatewayConfiguration.WEB_COMMON_PACKAGE;

/**
 * Configuration des pages et JsonView pour la gateway
 */
@Configuration
@Import({CommonConfig.class, SwaggerConfiguration.class})
@ComponentScan(basePackages = {COMMON_PACKAGE, WEB_COMMON_PACKAGE})
public class GatewayConfiguration extends WebConfig {
  static final String COMMON_PACKAGE = "co.fractalin.finpay.commons";
  static final String WEB_COMMON_PACKAGE = "co.fractalin.finpay.web.commons";
}
