package co.fractalin.finpay.gateway.service;

import co.fractalin.finpay.commons.exception.NotFoundException;
import co.fractalin.finpay.commons.exception.UserNotFoundException;
import co.fractalin.finpay.commons.log.LogArgumentsAndResult;
import co.fractalin.finpay.commons.machine.StateMachine;
import co.fractalin.finpay.gateway.repository.UserRepository;
import co.fractalin.finpay.model.enumeration.UserStatus;
import co.fractalin.finpay.model.security.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static co.fractalin.finpay.model.enumeration.UserStatus.ACTIVATED;
import static co.fractalin.finpay.model.enumeration.UserStatus.TO_BE_ACTIVATED;
import static co.fractalin.finpay.model.enumeration.UserStatus.INACTIVE;



@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {

  private static final StateMachine<User, UserStatus> STATE_MACHINE = StateMachine.<UserStatus>builder()

      .from(TO_BE_ACTIVATED).to(ACTIVATED)
      .from(ACTIVATED).to(INACTIVE)
      .from(INACTIVE).to(ACTIVATED)
      .build(User::getStatus, User::setStatus);
  private final UserRepository repository;

  public User findById(String id) {
    return repository.findById(id)

        .orElseThrow(() -> new NotFoundException(User.class, id));
  }

  public User findByLogin(String login) {
    return repository.findByUsername(login)
        .orElseThrow(() -> new UserNotFoundException(login));
  }

  @Transactional
  @LogArgumentsAndResult
  public User activate(User user) {
    return updateStatus(user, ACTIVATED);
  }



  private User updateStatus(User user, UserStatus status) {
    User updatedUser = STATE_MACHINE.changeStatus(user, status);

    return repository.save(updatedUser);
  }

  @Transactional
  @LogArgumentsAndResult
  public User disactivate(User user) {
    return updateStatus(user, INACTIVE);
  }


}
